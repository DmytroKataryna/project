package io.asphalte.android.ui.post;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.models.Post;
import io.asphalte.android.ui.custom.ErrorDialog;

/**
 * Created by dmytro on 11/20/15. Project : Asphalte
 */
public class CreatePostActivity extends AppCompatActivity implements View.OnClickListener, SaveCallback {//AdapterView.OnItemSelectedListener,

    private Post post;
    private ParseUser user;

    private EditText messageEditText, titleEditText;
    private AlertDialog errorDialog;
    private ProgressBar progressBar;

    private TextView visibilityStatusTextView, changeVisibilityTextView;
    private boolean isPrivate;

    public static void start(Context context_) {
        context_.startActivity(new Intent(context_, CreatePostActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        user = ParseUser.getCurrentUser();
        post = new Post();
        initToolbar();
        initViews();
    }

    private void initViews() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        titleEditText = (EditText) findViewById(R.id.titleEditText);
        messageEditText = (EditText) findViewById(R.id.messageEditText);
        changeVisibilityTextView = (TextView) findViewById(R.id.changeVisibilityTextView);
        visibilityStatusTextView = (TextView) findViewById(R.id.visibilityStatusTextView);

        changeVisibilityTextView.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_post_activity_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.getItemId() == R.id.action_post) {
                View view = MenuItemCompat.getActionView(item).findViewById(R.id.menuTextViewToolbar);
                if (view != null) {
                    ((TextView) view).setText(getResources().getText(R.string.fontello_icon_plane));
                    ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "fontello_social.ttf"));
                    view.setOnClickListener(this);
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void validateFields() {
        if (messageEditText.getText().toString().length() > Constants.ZERO)
            savePost();
        else
            showAlertDialog("Text field is required");  // TODO цей текст буде змінений , тому я ще не виносив його в string.xml
    }

    private void savePost() {
        progressBar.setVisibility(View.VISIBLE);
        post.setText(messageEditText.getText().toString());
        post.setTitle(titleEditText.getText().toString());
        post.setPrivate(isPrivate);
        post.setOwner(user);
        post.saveInBackground(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changeVisibilityTextView:
                changeVisibilityStatus(isPrivate);
                break;
            case R.id.errorDialogOkButton:
                errorDialog.cancel();
                break;
            case R.id.menuTextViewToolbar:
                validateFields();
                break;
        }
    }

    private void changeVisibilityStatus(boolean isPrivate) {
        if (isPrivate)
            visibilityStatusTextView.setText(getResources().getString(R.string.post_public_mode_text));
        else
            visibilityStatusTextView.setText(getResources().getString(R.string.post_private_mode_text));

        this.isPrivate = !isPrivate;
    }

    /**
     * triggered when post is stored into local database , than just close activity
     */
    @Override
    public void done(ParseException e) {
        progressBar.setVisibility(View.GONE);
        finish();
    }

    protected void showAlertDialog(String title) {
        errorDialog = new ErrorDialog(this).createErrorDialog(title);
        errorDialog.show();
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.btn_close);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView textView = ((TextView) mToolbar.findViewById(R.id.toolbarTitle));
        textView.setText(user.getUsername());
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}