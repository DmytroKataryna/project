package io.asphalte.android.ui.authorization;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import io.asphalte.android.R;
import io.asphalte.android.helpers.KeyboardHelper;
import io.asphalte.android.ui.MainActivity;
import io.asphalte.android.ui.custom.ForgotDialog;

/**
 * Created by dmytro on 11/18/15. Project : Asphalte
 */
public class LoginActivity extends AuthorizationActivity implements View.OnClickListener, LogInCallback, RequestPasswordResetCallback {

    private EditText nameEditText, passwordEditText;
    private Button signInButton, facebookSignInView;
    private TextView forgotPassTextView;
    private ForgotDialog forgotDialog;

    public static final int MIN_LENGTH = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initLoginToolbar();
        initViews();
    }

    private void initLoginToolbar() {
        initToolbar();
        setToolbarText(getString(R.string.sign_in_text));
    }

    private void initViews() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        signInButton = (Button) findViewById(R.id.signInButton);
        facebookSignInView = (Button) findViewById(R.id.facebookInButton);

        forgotPassTextView = (TextView) findViewById(R.id.forgotPassTextView);

        signInButton.setOnClickListener(this);
        facebookSignInView.setOnClickListener(this);
        forgotPassTextView.setOnClickListener(this);

        setViewsFont();
    }

    private void setViewsFont() {
        Typeface font = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
        ((TextView) findViewById(R.id.nameTitle)).setTypeface(font);
        ((TextView) findViewById(R.id.passTitle)).setTypeface(font);
        ((TextView) findViewById(R.id.orTitle)).setTypeface(font);

        nameEditText.setTypeface(font);
        passwordEditText.setTypeface(font);
        signInButton.setTypeface(font);
        facebookSignInView.setTypeface(font);
    }

    @Override
    public void onClick(View v) {
        KeyboardHelper.hide(this);
        switch (v.getId()) {
            case R.id.signInButton:
                signIn(nameEditText.getText().toString(), passwordEditText.getText().toString());
                break;
            case R.id.facebookInButton:
                facebookLogin();
                break;
            case R.id.errorDialogOkButton:
                errorDialog.cancel();
                break;
            case R.id.forgotPassTextView:
                showForgotPassDialog();
                break;
            case R.id.forgotDialogSendButton:
                forgotDialog.cancel();
                KeyboardHelper.hide(this);
                ParseUser.requestPasswordResetInBackground((forgotDialog).getEmailText(), this);
                break;
        }
    }

    private void showForgotPassDialog() {
        forgotDialog = new ForgotDialog(this);
        forgotDialog.createForgotDialog().show();
    }

    private void signIn(String userName, String password) {
        if (userName.length() >= MIN_LENGTH & password.length() >= MIN_LENGTH) {
            onLoginStart();
            ParseUser.logInInBackground(userName, password, this);
        }
    }

    @Override
    public void done(ParseUser user, ParseException e) {
        onLoginEnd();
        if (user != null) {
            registerPushNotification(user);
            MainActivity.start(getApplicationContext());
        } else
            parseException(e);
    }

    private void onLoginStart() {
        showProgressBar();
        signInButton.setEnabled(false);
    }

    private void onLoginEnd() {
        hideProgressBar();
        signInButton.setEnabled(true);
    }

    @Override
    public void done(ParseException e) {
        if (e == null)
            showAlertDialog(getResources().getString(R.string.request_password_success_text));
        else
            showAlertDialog(e);
    }
}
