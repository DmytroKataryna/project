package io.asphalte.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.parse.ParseUser;
import io.asphalte.android.R;
import io.asphalte.android.models.User;
import io.asphalte.android.ui.authorization.StartActivity;
import io.asphalte.android.ui.custom.LoginDialog;

/**
 * Created by dmytro on 12/7/15. Project : Asphalte
 */
public class LoginAlertDialogActivity extends AppCompatActivity implements View.OnClickListener {

    public AlertDialog loginDialog;
    protected User user;
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = new User(ParseUser.getCurrentUser());
    }

    @Override
    protected void onResume() {
        super.onResume();
        loginDialog = new LoginDialog(this).createLoginDialog();
    }

    protected void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected boolean checkIfAuthorized() {
        return user.checkIfAuthorized();
    }

    public void showLoginDialog() {
        loginDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginDialogPositiveButton:
                startActivity(new Intent(getApplicationContext(), StartActivity.class));
            case R.id.loginDialogNegativeButton:
                loginDialog.cancel();
                break;
        }
    }
}
