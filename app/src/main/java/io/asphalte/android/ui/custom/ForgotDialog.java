package io.asphalte.android.ui.custom;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import io.asphalte.android.Constants;
import io.asphalte.android.R;

/**
 * Created by dmytro on 11/27/15. Project : Asphalte
 */
public class ForgotDialog extends AlertDialog {

    private Activity mActivity;
    private View.OnClickListener listener;
    private EditText forgotEmailText;

    public ForgotDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
        this.listener = (View.OnClickListener) activity;
    }

    public AlertDialog createForgotDialog() {
        View title = getLayoutInflater().inflate(R.layout.dialog_forgot_pass_title_view, null);
        View view = getLayoutInflater().inflate(R.layout.dialog_forgot_pass_msg_view, null);
        view.findViewById(R.id.forgotDialogSendButton).setOnClickListener(listener);

        forgotEmailText = (EditText) view.findViewById(R.id.forgotEmailEditText);

        forgotEmailText.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "Roboto-Regular.ttf"));

        setCustomTitle(title);
        setView(view);
        setCancelable(true);
        return this;
    }

    public String getEmailText() {
        try {
            return forgotEmailText.getText().toString();
        } catch (Exception e) {
            return Constants.EMPTY;
        }
    }

}
