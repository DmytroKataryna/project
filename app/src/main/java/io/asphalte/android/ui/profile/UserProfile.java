package io.asphalte.android.ui.profile;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.adapters.PostsAdapter;
import io.asphalte.android.adapters.ProfilePostsAdapter;
import io.asphalte.android.models.Post;
import io.asphalte.android.ui.LoginAlertDialogActivity;
import io.asphalte.android.ui.custom.ReportDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmytro on 11/26/15. Project : Asphalte
 */
public class UserProfile extends LoginAlertDialogActivity implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener, View.OnClickListener, SaveCallback, FindCallback<Post> {

    private String profileUserID, profileUserName;
    private PostsAdapter mAdapter;
    private SuperRecyclerView mListView;
    private ProgressBar progressBar;
    private ReportDialog reportDialog;

    private Button followButton;
    private boolean isFollowing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        profileUserID = getIntent().getStringExtra(Constants.PARSE.OBJECT_ID);
        profileUserName = getIntent().getStringExtra(Constants.PARSE.NAME);
        reportDialog = new ReportDialog(this).createReportDialog();
        checkIfFollowed();
        initToolbar();
        initViews();
    }

    private void initViews() {
        mAdapter = new ProfilePostsAdapter(new ArrayList<Post>(), this);

        mListView = (SuperRecyclerView) findViewById(R.id.superListView);
        mListView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mListView.setRefreshListener(this);
        mListView.setupMoreListener(this, Constants.LIST_ITEMS_TO_LOAD);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        onRefresh();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.followButtonToolbar:
                followButtonEnable(false);
                followButtonInteraction(isFollowing);
                break;
            case R.id.reportDialogButton:
                sendReport(reportDialog.getReportText());
                break;
        }
    }

    @Override
    public void onRefresh() {
        if (mAdapter == null) return;
        mListView.setupMoreListener(this, Constants.LIST_ITEMS_TO_LOAD);
        loadData(Constants.ZERO, Constants.LIST_ITEMS_TO_LOAD, true);
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        if (mAdapter == null) return;
        loadData(overallItemsCount, itemsBeforeMore, false);
    }

    private void loadData(int offset, int count, boolean resetData) {
        if (resetData)
            mAdapter.requestItemsRemoving();

        makeParseRequest(offset, count);
    }

    private void makeParseRequest(int offset, int count) {
        ParseQuery<Post> query = ParseQuery.getQuery(Constants.PARSE.POST);
        query.addDescendingOrder(Constants.PARSE.CREATED_AT);
        query.setLimit(count);
        query.setSkip(offset);
        query.whereEqualTo(Constants.PARSE.OWNER_ID, profileUserID);
        query.findInBackground(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_activity_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.getItemId() == R.id.follow) {
                View view = MenuItemCompat.getActionView(item).findViewById(R.id.followButtonToolbar);
                if (view != null) {
                    followButton = (Button) view;
                    followButton.setOnClickListener(this);
                    followButton.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"));
                    changeFollowButtonView(isFollowing);
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_hide:
                hideProfileUser();
                return true;
            case R.id.action_report:
                reportDialog.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void followButtonInteraction(boolean isFollowing) {
        if (!checkIfAuthorized()) {
            showLoginDialog();
            followButtonEnable(true);
            return;
        }

        checkIfFollowed();
        user.subscribeUser(profileUserID, isFollowing, this);
    }

    private void changeFollowButtonState() {
        isFollowing = !isFollowing;
        changeFollowButtonView(isFollowing);
    }

    private void changeFollowButtonView(boolean isFollowing) {
        if (!checkIfAuthorized()) return;
        if (isFollowing) {
            followButton.setBackgroundResource(R.drawable.button_white_background);
            followButton.setTextColor(getResources().getColor(R.color.black));
            followButton.setText(getResources().getString(R.string.following_text));
        } else {
            followButton.setBackgroundResource(R.drawable.button_background_with_white_frame);
            followButton.setTextColor(getResources().getColor(R.color.white));
            followButton.setText(getResources().getString(R.string.follow_text));
        }
    }

    private void checkIfFollowed() {
        if (checkIfAuthorized())
            isFollowing = user.getSubscribedUsers().contains(profileUserID);
    }

    private void updateMySavedData(List<Post> list) {
        mAdapter.updateData(list);

        if (list.size() == Constants.ZERO)
            mListView.removeMoreListener();
    }

    private void checkAdapter() {
        if (mListView.getAdapter() == null)
            mListView.setAdapter(mAdapter);
    }

    private void hideProgressBars() {
        progressBar.setVisibility(View.GONE);
        mListView.hideProgress();
        mListView.hideMoreProgress();
    }

    private void followButtonEnable(boolean enable) {
        followButton.setEnabled(enable);
    }

    private void hideProfileUser() {
        user.hideUserProfile(profileUserID);
    }


    private void sendReport(String reportText) {
        ParseObject report = new ParseObject(Constants.PARSE.REPORT);
        report.put(Constants.PARSE.FROM, user.getParseUser().getObjectId());
        report.put(Constants.PARSE.TO, profileUserID);
        report.put(Constants.PARSE.MSG, reportText);
        report.saveInBackground();
        reportDialog.cancel();
    }

    @Override
    public void done(List<Post> list, ParseException e) {
        checkAdapter();
        hideProgressBars();

        if (e == null)
            updateMySavedData(list);
    }

    @Override
    public void done(ParseException e) {
        if (e == null)
            changeFollowButtonState();

        followButtonEnable(true);
    }

    protected void initToolbar() {
        super.initToolbar();
        mToolbar.setNavigationIcon(R.drawable.btn_close);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView textView = ((TextView) mToolbar.findViewById(R.id.toolbarTitle));
        textView.setText(profileUserName);
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"));
    }
}

