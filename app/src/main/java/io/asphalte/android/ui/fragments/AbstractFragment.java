package io.asphalte.android.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.adapters.PostsAdapter;
import io.asphalte.android.helpers.PreferencesUtils;
import io.asphalte.android.models.Post;
import io.asphalte.android.ui.authorization.StartActivity;

import java.util.List;

/**
 * Created by dmytro on 11/25/15. Project : Asphalte
 */
public abstract class AbstractFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, OnMoreListener, FindCallback<Post>, View.OnClickListener {

    protected Activity mActivity;
    protected PostsAdapter mAdapter;
    protected SuperRecyclerView mListView;
    protected ProgressBar progressBar;
    protected ParseUser user;
    protected PreferencesUtils utils;

    protected TextView emptyTextView, unauthorizedTextView;
    protected Button unauthorizedButton;
    protected String emptyText;
    protected boolean isRecent;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
        user = ParseUser.getCurrentUser();
        utils = PreferencesUtils.get(context);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        if (isAuthorized() || isRecent) {
            view = inflater.inflate(R.layout.list_fragment, container, false);
            mListView = (SuperRecyclerView) view.findViewById(R.id.superListView);
            mListView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mListView.setRefreshListener(this);
            mListView.setupMoreListener(this, Constants.LIST_ITEMS_TO_LOAD);

            emptyTextView = ((TextView) (mListView.getEmptyView()).findViewById(R.id.empty));
            emptyTextView.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "Roboto-Regular.ttf"));
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            view = inflater.inflate(R.layout.unauthorized_fragment, container, false);
            unauthorizedTextView = (TextView) view.findViewById(R.id.unauthorizedTextView);
            unauthorizedButton = (Button) view.findViewById(R.id.unauthorizedButton);
            unauthorizedButton.setOnClickListener(this);
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        onRefresh();
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        if (mAdapter == null) return;
        loadData(overallItemsCount, itemsBeforeMore, false);
    }

    @Override
    public void onRefresh() {
        if (mAdapter == null) return;
        mListView.setupMoreListener(this, Constants.LIST_ITEMS_TO_LOAD);
        loadData(Constants.ZERO, Constants.LIST_ITEMS_TO_LOAD, true);
    }

    protected void loadData(int offset, int count, boolean resetData) {
        if (resetData)
            mAdapter.requestItemsRemoving();

        setEmptyViewLoadingText();
        makeParseRequest(offset, count);
    }

    protected abstract void makeParseRequest(int offset, int count);

    @Override
    public void done(List<Post> list, ParseException e) {
        if (list == null) return;
        if (list.isEmpty()) setEmptyText();

        checkAdapter();
        hideProgressBars();

        if (e == null)
            updateMySavedData(list);
    }

    public void setEmptyText() {
        emptyTextView.setText(emptyText);
    }

    private void setEmptyViewLoadingText() {
        emptyTextView.setText(getResources().getString(R.string.loading_text));
    }

    protected void updateMySavedData(List<Post> list) {
        mAdapter.updateData(list);

        if (list.size() == Constants.ZERO)
            mListView.removeMoreListener();
    }

    protected void checkAdapter() {
        if (mListView.getAdapter() == null)
            mListView.setAdapter(mAdapter);
    }

    protected void hideProgressBars() {
        progressBar.setVisibility(View.GONE);
        mListView.hideProgress();
        mListView.hideMoreProgress();
    }

    protected boolean isAuthorized() {
        return user != null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.unauthorizedButton)
            startActivity(new Intent(getContext(), StartActivity.class));
    }
}
