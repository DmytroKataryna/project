package io.asphalte.android.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.asphalte.android.R;
import io.asphalte.android.adapters.PagerAdapter;
import io.asphalte.android.helpers.PreferencesUtils;
import io.asphalte.android.ui.fragments.MyPostsFragment;
import io.asphalte.android.ui.fragments.RecentPostsFragment;
import io.asphalte.android.ui.fragments.SavedPostsFragment;
import io.asphalte.android.ui.fragments.SubscribedPostsFragment;
import io.asphalte.android.ui.post.CreatePostActivity;
import io.asphalte.android.ui.settings.SettingActivity;

public class MainActivity extends LoginAlertDialogActivity implements TabLayout.OnTabSelectedListener, View.OnClickListener {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    public static void start(Context context_) {
        context_.startActivity(new Intent(context_, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();
        initViews();
        initViewPagerAndTabs();
    }

    protected void initToolbar() {
        super.initToolbar();
    }

    private void initViews() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    private void initViewPagerAndTabs() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(RecentPostsFragment.createInstance(), getResources().getString(R.string.fontello_icon_megaphone));
        pagerAdapter.addFragment(SubscribedPostsFragment.createInstance(), getResources().getString(R.string.fontello_icon_chat));
        pagerAdapter.addFragment(MyPostsFragment.createInstance(), getResources().getString(R.string.fontello_icon_comment));
        pagerAdapter.addFragment(SavedPostsFragment.createInstance(), getResources().getString(R.string.fontello_icon_heart));
        viewPager.setAdapter(pagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(this);
        tabLayoutSetTypeface(tabLayout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (checkIfAuthorized()) {
            getMenuInflater().inflate(R.menu.main_activity_menu, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.fab:
                if (checkIfAuthorized())
                    CreatePostActivity.start(getApplicationContext());
                else
                    showLoginDialog();
                break;
        }
    }

    /**
     * Find tab textView and set it typeface
     */
    private void tabLayoutSetTypeface(TabLayout tabLayout) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fontello.ttf");
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            ((TextView) ((LinearLayout) ((LinearLayout) tabLayout.getChildAt(0)).getChildAt(i)).getChildAt(1)).setTypeface(font);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        PreferencesUtils.get(getApplicationContext()).setSelectedTabID(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }
}
