package io.asphalte.android.ui.post;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.parse.CountCallback;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.helpers.PreferencesUtils;
import io.asphalte.android.models.Post;
import io.asphalte.android.ui.custom.DeleteDialog;

/**
 * Created by dmytro on 11/27/15. Project : Asphalte
 */
public class OwnPostDetailsActivity extends AbstractPostDetails implements View.OnClickListener {

    private MenuItem privacyMenuItem;
    private DeleteDialog dialog;

    private TextView saveNumberTextView, postTextView, postStatusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_own_post_details);
        String postID = retrievePostID(getIntent());
        cancelNotification(postID);
        initToolbar();
        initViews();
        loadPost(postID);
        preFetchDeepLink(postID);
    }

    private void cancelNotification(String postID) {
        PreferencesUtils utils = PreferencesUtils.get(getApplicationContext());
        if (utils.getPushNotificationID(postID) != -1)
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(utils.getPushNotificationID(postID));
    }

    private String retrievePostID(Intent intent) {
        try {
            if (intent.getExtras().getString(Constants.PARSE.KEY_PUSH_DATA) == null)
                throw new JSONException(Constants.EMPTY);
            return new JSONObject(intent.getExtras().getString(Constants.PARSE.KEY_PUSH_DATA)).getString(Constants.PARSE.PUSH_POST_ID);
        } catch (JSONException e) {
            return intent.getStringExtra(Constants.PARSE.OBJECT_ID);
        }
    }

    private void initViews() {
        Typeface robotoFont = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        postTextView = (TextView) findViewById(R.id.postTextView);
        dateTextView = (TextView) findViewById(R.id.dateTextView);
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        postStatusTextView = (TextView) findViewById(R.id.postStatusTextView);
        saveNumberTextView = (TextView) findViewById(R.id.saveNumberTextView);

        titleTextView.setTypeface(Typeface.SERIF, Typeface.BOLD);
        postTextView.setTypeface(Typeface.SERIF);
        postStatusTextView.setTypeface(robotoFont);
        saveNumberTextView.setTypeface(robotoFont);
    }

    protected void loadPost(String currentPostID) {
        ParseQuery<Post> query = ParseQuery.getQuery(Constants.PARSE.POST);
        query.getInBackground(currentPostID, new GetCallback<Post>() {
            public void done(Post object, ParseException e) {
                if (e == null) {
                    post = object;
                    updatePostViews();
                }
            }
        });
    }

    private void updatePostViews() {
        updatePostPrivacyStatus();
        setPostDate();
        setPostTitle();
        setSavedNumber();
        postTextView.setText(post.getText());

    }

    private void updatePostPrivacyStatus() {
        postStatusTextView.setText(post.isPrivate() ? getResources().getString(R.string.private_text) : getResources().getString(R.string.public_text));
        privacyMenuItem.setTitle(post.isPrivate() ? getString(R.string.menu_public) : getString(R.string.menu_private));
    }

    private void setSavedNumber() {
        user.countNumbersOfSavedPosts(post.getObjectId(), new CountCallback() {
            @Override
            public void done(int count, ParseException e) {
                findViewById(R.id.saveImageView).setVisibility(View.VISIBLE);
                saveNumberTextView.setText(String.valueOf(count));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_own_post_details, menu);
        privacyMenuItem = menu.findItem(R.id.action_privacy);
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.getItemId() == R.id.share) {
                View view = MenuItemCompat.getActionView(item).findViewById(R.id.menuTextViewToolbar);
                if (view != null) {
                    ((TextView) view).setText(getResources().getText(R.string.fontello_icon_share));
                    ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "fontello_social.ttf"));
                    view.setOnClickListener(this);
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_privacy:
                item.setEnabled(false);
                changePostPrivacyStatus(item);
                return true;
            case R.id.action_delete:
                showDeleteDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteDialog() {
        dialog = new DeleteDialog(this);
        dialog.createDeleteDialog().show();
    }

    private void changePostPrivacyStatus(final MenuItem item) {
        post.setPrivate(!post.isPrivate());
        post.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                updatePostPrivacyStatus();
                item.setEnabled(true);
            }
        });
    }

    protected void initToolbar() {
        super.initToolbar();
        mToolbar.setNavigationIcon(R.drawable.btn_close);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView textView = ((TextView) mToolbar.findViewById(R.id.toolbarTitle));
        textView.setText(user.getParseUser().getUsername());
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancelButton:
                dialog.cancel();
                break;
            case R.id.deleteButton:
                deletePost();
                break;
            case R.id.menuTextViewToolbar:
                sendShareIntent();
                break;
        }
    }

    private void deletePost() {
        post.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
                finish();
            }
        });
    }
}
