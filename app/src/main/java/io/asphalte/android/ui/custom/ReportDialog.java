package io.asphalte.android.ui.custom;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import io.asphalte.android.Constants;
import io.asphalte.android.R;

/**
 * Created by dmytro on 11/27/15. Project : Asphalte
 */
public class ReportDialog extends AlertDialog {

    private Activity mActivity;
    private View.OnClickListener listener;
    private EditText reportEmailText;

    public ReportDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
        this.listener = (View.OnClickListener) activity;
    }

    public ReportDialog createReportDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_report_msg_view, null);
        view.findViewById(R.id.reportDialogButton).setOnClickListener(listener);

        reportEmailText = (EditText) view.findViewById(R.id.reportEditText);

        reportEmailText.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "Roboto-Regular.ttf"));

        setView(view);
        setCancelable(true);
        return this;
    }

    public String getReportText() {
        try {
            return reportEmailText.getText().toString();
        } catch (Exception e) {
            return Constants.EMPTY;
        }
    }

}
