package io.asphalte.android.ui.custom;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import io.asphalte.android.R;

/**
 * Created by dmytro on 11/19/15. Project : Asphalte
 */
public class ErrorDialog extends AlertDialog {

    private Activity mActivity;
    private View.OnClickListener listener;

    public ErrorDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
        this.listener = (View.OnClickListener) activity;
    }

    public AlertDialog createErrorDialog(String title) {
        View view = getLayoutInflater().inflate(R.layout.dialog_authorization_errors_view, null);
        view.findViewById(R.id.errorDialogOkButton).setOnClickListener(listener);

        TextView msg = (TextView) view.findViewById(R.id.msgTextView);
        msg.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "Roboto-Regular.ttf"));
        msg.setText(title);

        setCancelable(true);
        setView(view);
        return this;
    }
}