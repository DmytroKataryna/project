package io.asphalte.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.ParseQuery;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.adapters.RecentPostsAdapter;
import io.asphalte.android.models.Post;

import java.util.ArrayList;

/**
 * Created by dmytro on 11/18/15. Project : Asphalte
 */
public class SubscribedPostsFragment extends AbstractFragment {

    public static SubscribedPostsFragment createInstance() {
        return new SubscribedPostsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (isAuthorized()) {
            emptyText = getResources().getString(R.string.empty_list_follow_text);
            mAdapter = new RecentPostsAdapter(new ArrayList<Post>(), this);
            mListView.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    protected void makeParseRequest(int offset, int count) {
        ParseQuery<Post> query = ParseQuery.getQuery(Constants.PARSE.POST);
        query.whereEqualTo(Constants.PARSE.IS_PRIVATE, false);
        query.addDescendingOrder(Constants.PARSE.CREATED_AT);
        query.setLimit(count);
        query.setSkip(offset);

        query.whereContainedIn(Constants.PARSE.OWNER_ID, (ArrayList<String>) user.get(Constants.PARSE.SUBSCRIBED_USERS));
        query.whereNotContainedIn(Constants.PARSE.OWNER_ID, (ArrayList<String>) user.get(Constants.PARSE.HIDDEN_USERS));

        query.findInBackground(this);
    }
}
