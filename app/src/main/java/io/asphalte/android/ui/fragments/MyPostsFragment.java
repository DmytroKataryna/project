package io.asphalte.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.ParseQuery;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.adapters.MyPostsAdapter;
import io.asphalte.android.models.Post;

import java.util.ArrayList;

/**
 * Created by dmytro on 11/18/15. Project : Asphalte
 */
public class MyPostsFragment extends AbstractFragment {

    public static MyPostsFragment createInstance() {
        return new MyPostsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (isAuthorized()) {
            emptyText = getResources().getString(R.string.empty_list_my_text);
            mAdapter = new MyPostsAdapter(new ArrayList<Post>(), this);
            mListView.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    protected void makeParseRequest(int offset, int count) {
        ParseQuery<Post> query = ParseQuery.getQuery(Constants.PARSE.POST);
        query.addDescendingOrder(Constants.PARSE.CREATED_AT);
        query.setLimit(count);
        query.setSkip(offset);

        query.whereEqualTo(Constants.PARSE.OWNER_ID, user.getObjectId());

        query.findInBackground(this);
    }
}
