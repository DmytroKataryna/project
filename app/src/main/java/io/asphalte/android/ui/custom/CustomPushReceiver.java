package io.asphalte.android.ui.custom;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import io.asphalte.android.Constants;
import io.asphalte.android.helpers.PreferencesUtils;
import io.asphalte.android.ui.post.OwnPostDetailsActivity;

/**
 * Created by dmytro on 12/11/15. Project : Asphalte
 */
public class CustomPushReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected Class<? extends Activity> getActivity(Context context, Intent intent) {
        try {
            if (intent.getExtras().getString(Constants.PARSE.KEY_PUSH_DATA) == null)
                throw new JSONException(Constants.EMPTY);
            else if (!new JSONObject(intent.getExtras().getString(Constants.PARSE.KEY_PUSH_DATA)).has(Constants.PARSE.PUSH_POST_ID))
                throw new JSONException(Constants.EMPTY);
            else
                return OwnPostDetailsActivity.class;
        } catch (JSONException e) {
            return super.getActivity(context, intent);
        }
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        JSONObject pushData = null;
        String objectID = null;
        String userID = null;
        try {
            pushData = new JSONObject(intent.getStringExtra(KEY_PUSH_DATA));
            objectID = pushData.getString(Constants.PARSE.PUSH_POST_ID);
            userID = pushData.getString(Constants.PARSE.PUSH_USER_ID);
        } catch (JSONException e) {
            Log.e(Constants.APP_TAG, "Unexpected JSONException when receiving push data: ", e);
        }

        String action = null;
        if (pushData != null) {
            action = pushData.optString("action", null);
        }
        if (action != null) {
            Bundle extras = intent.getExtras();
            Intent broadcastIntent = new Intent();
            broadcastIntent.putExtras(extras);
            broadcastIntent.setAction(action);
            broadcastIntent.setPackage(context.getPackageName());
            context.sendBroadcast(broadcastIntent);
        }

        Notification notification = getNotification(context, intent);

        if (notification != null) {
            showNotification(context, notification, objectID, userID);
        }
    }

    private void showNotification(Context context, Notification notification, String postID, String userID) {
        if (context != null && notification != null) {

            if (ParseUser.getCurrentUser() != null && ParseUser.getCurrentUser().getObjectId().equals(userID)) {
                NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                cancelNotification(context, postID);
                int notificationId = (int) System.currentTimeMillis();
                PreferencesUtils.get(context).setPushNotificationID(postID, notificationId);

                try {
                    nm.notify(notificationId, notification);
                } catch (SecurityException e) {
                    notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND;
                    nm.notify(notificationId, notification);
                }
            }
        }
    }

    private void cancelNotification(Context context, String pushID) {
        PreferencesUtils utils = PreferencesUtils.get(context);
        if (utils.getPushNotificationID(pushID) != -1)
            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(utils.getPushNotificationID(pushID));
    }
}
