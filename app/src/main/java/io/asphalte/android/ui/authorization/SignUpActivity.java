package io.asphalte.android.ui.authorization;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.helpers.KeyboardHelper;
import io.asphalte.android.helpers.Validator;
import io.asphalte.android.ui.MainActivity;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by dmytro on 11/18/15. Project : Asphalte
 */
public class SignUpActivity extends AuthorizationActivity implements View.OnClickListener, SignUpCallback {

    private EditText nameEditText, emailEditText, passwordEditText;
    private Button signUpButton, facebookSignUpView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initSignUpToolbar();
        initViews();
    }

    private void initSignUpToolbar() {
        initToolbar();
        setToolbarText(getString(R.string.sign_up_text));
    }

    private void initViews() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        signUpButton = (Button) findViewById(R.id.signUpButton);
        facebookSignUpView = (Button) findViewById(R.id.facebookUpButton);

        signUpButton.setOnClickListener(this);
        facebookSignUpView.setOnClickListener(this);

        setViewsFont();
    }

    private void setViewsFont() {
        Typeface font = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
        ((TextView) findViewById(R.id.nameTitle)).setTypeface(font);
        ((TextView) findViewById(R.id.emailTitle)).setTypeface(font);
        ((TextView) findViewById(R.id.passTitle)).setTypeface(font);
        ((TextView) findViewById(R.id.orTitle)).setTypeface(font);

        nameEditText.setTypeface(font);
        emailEditText.setTypeface(font);
        passwordEditText.setTypeface(font);
        signUpButton.setTypeface(font);
        facebookSignUpView.setTypeface(font);
    }

    @Override
    public void onClick(View v) {
        KeyboardHelper.hide(this);
        switch (v.getId()) {
            case R.id.signUpButton:
                register();
                break;
            case R.id.facebookUpButton:
                facebookLogin();
                break;
            case R.id.errorDialogOkButton:
                errorDialog.cancel();
                break;
        }
    }

    private void register() {
        if (validateSignUpFields(nameEditText.getText(), emailEditText.getText(), passwordEditText.getText())) {
            onSignUpStart();
            final ParseUser user = new ParseUser();
            user.setUsername(nameEditText.getText().toString());
            user.setEmail(emailEditText.getText().toString());
            user.setPassword(passwordEditText.getText().toString());
            user.put(Constants.PARSE.LANGUAGE, Locale.getDefault().getDisplayLanguage());
            user.put(Constants.PARSE.LANGUAGE_ID, Locale.getDefault().getISO3Language());
            user.addAll(Constants.PARSE.HIDDEN_USERS, new ArrayList<>());
            user.addAll(Constants.PARSE.SUBSCRIBED_USERS, new ArrayList<>());
            user.addAll(Constants.PARSE.SAVED_IDS, new ArrayList<>());
            user.signUpInBackground(this);
        }
    }

    private boolean validateSignUpFields(Editable nameText, Editable emailText, Editable passwordText) {
        String error = Validator.validate(nameText.toString(), Validator.NAME);
        if (error == null) {
            error = Validator.validate(emailText.toString(), Validator.EMAIL);
            if (error == null) {
                error = Validator.validate(passwordText.toString(), Validator.PASSWORD);
            }
        }

        if (error != null) showAlertDialog(error);

        return error == null;
    }

    @Override
    public void done(ParseException e) {
        onSignUpEnd();
        if (e == null)
            MainActivity.start(getApplicationContext());
        else
            parseException(e);
    }

    private void onSignUpStart() {
        showProgressBar();
        signUpButton.setEnabled(false);
    }

    private void onSignUpEnd() {
        hideProgressBar();
        signUpButton.setEnabled(true);
    }
}
