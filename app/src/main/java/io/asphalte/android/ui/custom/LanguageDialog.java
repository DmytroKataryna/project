package io.asphalte.android.ui.custom;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ListView;

import io.asphalte.android.R;
import io.asphalte.android.adapters.DialogLanguageAdapter;

/**
 * Created by dmytro on 12/3/15. Project : Asphalte
 */
public class LanguageDialog extends AlertDialog {

    private Activity mActivity;

    public LanguageDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
    }

    public AlertDialog createLanguageDialog() {
        View title = getLayoutInflater().inflate(R.layout.dialog_language_title_view, null);
        View view = getLayoutInflater().inflate(R.layout.dialog_language_list_view, null);
        ListView filterListView = (ListView) view.findViewById(R.id.custom_list);
        filterListView.setAdapter(new DialogLanguageAdapter(mActivity, filterListView));

        setCustomTitle(title);
        setView(view);
        setCancelable(true);
        return this;
    }
}
