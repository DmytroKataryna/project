package io.asphalte.android.ui.authorization;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.helpers.FacebookHelper;
import io.asphalte.android.ui.MainActivity;
import io.asphalte.android.ui.custom.ErrorDialog;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dmytro on 11/20/15. Project : Asphalte
 */
public class AuthorizationActivity extends AppCompatActivity {

    protected List<String> permissions = Arrays.asList("public_profile", "email");

    protected Toolbar mToolbar;
    protected ProgressBar progressBar;
    protected AlertDialog errorDialog;

    protected void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.btn_close);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void setToolbarText(String text) {
        TextView textView = ((TextView) mToolbar.findViewById(R.id.toolbarTitle));
        textView.setText(text);
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    protected void facebookLogin() {
        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, permissions, new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException err) {
                        if (user == null)
                            showAlertDialog(err);
                        else if (user.isNew()) {
                            registerPushNotification(user);
                            FacebookHelper.signUpByFacebook(getApplicationContext());
                        } else {
                            registerPushNotification(user);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        }
                    }
                }
        );
    }


    protected void showAlertDialog(ParseException e) {
        try {
            showAlertDialog(e.getCause().getMessage());
        } catch (Exception exception) {
            showAlertDialog(e.getMessage());
        }
    }

    public void showAlertDialog(String title) {
        errorDialog = new ErrorDialog(this).createErrorDialog(title);
        errorDialog.show();
    }

    protected void parseException(ParseException e) {
        switch (e.getCode()) {
            case ParseException.USERNAME_TAKEN:
                showAlertDialog("Sorry, this user name has already been taken.");
                break;
            case ParseException.EMAIL_TAKEN:
                showAlertDialog("Sorry, this email has already been taken.");
                break;
            case ParseException.EMAIL_MISSING:
                showAlertDialog("Sorry, you must supply email to register.");
                break;
            case ParseException.USERNAME_MISSING:
                showAlertDialog("Sorry, you must supply username to register.");
                break;
            case ParseException.PASSWORD_MISSING:
                showAlertDialog("Sorry, you must supply password to register.");
                break;
            case ParseException.CONNECTION_FAILED:
                showAlertDialog("Sorry, connection failed.");
                break;
            default:
                showAlertDialog(e);
        }
    }

    protected void registerPushNotification(ParseUser user) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(Constants.PARSE.PUSH_USER_ID, user.getObjectId());
        installation.saveInBackground();
    }

    protected void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    protected void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

}
