package io.asphalte.android.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.ParseQuery;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.adapters.SavedPostsAdapter;
import io.asphalte.android.models.Post;

import java.util.ArrayList;

/**
 * Created by dmytro on 11/18/15. Project : Asphalte
 */
public class SavedPostsFragment extends AbstractFragment {

    public static SavedPostsFragment createInstance() {
        return new SavedPostsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (isAuthorized()) {
            emptyText = getResources().getString(R.string.empty_list_saved_text);
            mAdapter = new SavedPostsAdapter(new ArrayList<Post>(), this);
            mListView.setAdapter(mAdapter);
        }
        return view;
    }

    protected void makeParseRequest(int offset, int count) {
        ParseQuery<Post> query = ParseQuery.getQuery(Constants.PARSE.POST);
        query.addDescendingOrder(Constants.PARSE.CREATED_AT);
        query.setLimit(count);
        query.setSkip(offset);

        query.whereContainedIn(Constants.PARSE.OBJECT_ID, (ArrayList<String>) user.get(Constants.PARSE.SAVED_IDS));
        query.whereNotContainedIn(Constants.PARSE.OWNER_ID, (ArrayList<String>) user.get(Constants.PARSE.HIDDEN_USERS));

        query.findInBackground(this);
    }
}
