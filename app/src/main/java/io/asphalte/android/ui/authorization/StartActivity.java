package io.asphalte.android.ui.authorization;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.parse.ParseAnalytics;
import com.parse.ParseUser;
import io.asphalte.android.R;
import io.asphalte.android.ui.MainActivity;


/**
 * Created by dmytro on 11/18/15. Project : Asphalte
 */
public class StartActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView loginTextView, skipTextView;
    private Button startButton;

    public static void start(Context context_) {
        context_.startActivity(new Intent(context_, StartActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        redirectToMainActivity();
        initViews();
    }

    private void redirectToMainActivity() {
        if (ParseUser.getCurrentUser() != null)
            MainActivity.start(getApplicationContext());
    }

    private void initViews() {
        Typeface fontAdobe = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        Typeface fontRoboto = Typeface.createFromAsset(getAssets(), "Roboto-Light.ttf");
        Typeface fontRobotoMedium = Typeface.createFromAsset(getAssets(), "Roboto-Medium.ttf");
        ((TextView) findViewById(R.id.alreadyTextView)).setTypeface(fontRobotoMedium);

        startButton = (Button) findViewById(R.id.startedButton);
        loginTextView = (TextView) findViewById(R.id.loginTextView);
        skipTextView = (TextView) findViewById(R.id.skipTextView);

        startButton.setOnClickListener(this);
        loginTextView.setOnClickListener(this);
        skipTextView.setOnClickListener(this);

        startButton.setTypeface(fontAdobe);
        loginTextView.setTypeface(fontRobotoMedium);
        skipTextView.setTypeface(fontRoboto);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startedButton:
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
                break;
            case R.id.loginTextView:
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                break;
            case R.id.skipTextView:
                MainActivity.start(getApplicationContext());
                break;
        }
    }
}
