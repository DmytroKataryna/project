package io.asphalte.android.ui.custom;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import io.asphalte.android.R;

/**
 * Created by dmytro on 11/27/15. Project : Asphalte
 */
public class DeleteDialog extends AlertDialog {

    private Activity mActivity;
    private View.OnClickListener listener;

    public DeleteDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
        this.listener = (View.OnClickListener) activity;
    }

    public DeleteDialog createDeleteDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_delete_post_view, null);
        view.findViewById(R.id.cancelButton).setOnClickListener(listener);
        view.findViewById(R.id.deleteButton).setOnClickListener(listener);

        ((TextView) view.findViewById(R.id.msgTextView))
                .setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "Roboto-Regular.ttf"));

        setView(view);
        setCancelable(false);
        return this;
    }
}