package io.asphalte.android.ui.post;


import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import io.asphalte.android.R;
import io.asphalte.android.helpers.BranchIO;
import io.asphalte.android.helpers.ShareIntentHelper;
import io.asphalte.android.models.Post;
import io.asphalte.android.ui.LoginAlertDialogActivity;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by dmytro on 12/15/15. Project : Asphalte
 */
public abstract class AbstractPostDetails extends LoginAlertDialogActivity {

    protected Post post;
    protected TextView toolbarTextView, titleTextView, dateTextView;

    private String deepLinkBranchIO;

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    protected void setPostTitle() {
        if (post.getTitle() != null) {
            titleTextView.setVisibility(View.VISIBLE);
            titleTextView.setText(post.getTitle());
        }
    }

    protected abstract void loadPost(String currentPostID);

    protected void initToolbarTitle(String postOwnerUsername) {
        toolbarTextView.setText(postOwnerUsername);
    }

    protected void setPostDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
        dateTextView.setText(String.format("Written on %s", sdfDate.format(post.getCreatedAt())));
    }

    protected void preFetchDeepLink(String postID) {
        BranchIO.generateShortUrl(getApplicationContext(), postID, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                deepLinkBranchIO = url;
            }
        });
    }

    protected void sendShareIntent() {
        final Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_intent_extra_text));
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_intent_extra_text) + " " + deepLinkBranchIO);
        startActivity(ShareIntentHelper.generateCustomChooserIntent(getApplicationContext(), sendIntent));
    }
}
