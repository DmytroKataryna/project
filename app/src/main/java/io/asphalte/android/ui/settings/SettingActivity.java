package io.asphalte.android.ui.settings;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.parse.ParseUser;

import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.ui.authorization.StartActivity;
import io.asphalte.android.ui.custom.InviteFriendDialog;
import io.asphalte.android.ui.custom.LanguageDialog;

/**
 * Created by dmytro on 12/3/15. Project : Asphalte
 */
public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int REQUEST_INVITE = 18;

    public ParseUser user;
    public TextView userNameTextView, userEmailTextView, defaultLanguageTextView, languageTextView, sendFeedbackTextView, inviteFriendsTextView, privacyPolicyTextView, signOutTextView;
    public LanguageDialog languageDialog;
    public InviteFriendDialog inviteFriendDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        user = ParseUser.getCurrentUser();
        initToolbar();
        initViews();
        updateViewsData();
    }

    private void initViews() {
        Typeface font = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        userNameTextView = (TextView) findViewById(R.id.userNameTextView);
        userEmailTextView = (TextView) findViewById(R.id.userEmailTextView);
        defaultLanguageTextView = (TextView) findViewById(R.id.defaultLanguageTextView);
        languageTextView = (TextView) findViewById(R.id.languageTextView);
        sendFeedbackTextView = (TextView) findViewById(R.id.sendFeedbackTextView);
        inviteFriendsTextView = (TextView) findViewById(R.id.inviteFriendsTextView);
        privacyPolicyTextView = (TextView) findViewById(R.id.privacyPolicyTextView);
        signOutTextView = (TextView) findViewById(R.id.signOutTextView);

        userNameTextView.setTypeface(font);
        userEmailTextView.setTypeface(font);
        defaultLanguageTextView.setTypeface(font);
        languageTextView.setTypeface(font);
        sendFeedbackTextView.setTypeface(font);
        inviteFriendsTextView.setTypeface(font);
        privacyPolicyTextView.setTypeface(font);
        signOutTextView.setTypeface(font);

        defaultLanguageTextView.setOnClickListener(this);
        languageTextView.setOnClickListener(this);
        sendFeedbackTextView.setOnClickListener(this);
        inviteFriendsTextView.setOnClickListener(this);
        privacyPolicyTextView.setOnClickListener(this);
        signOutTextView.setOnClickListener(this);

        ((TextView) findViewById(R.id.settingsGeneralMenuTextView)).setTypeface(font);
        ((TextView) findViewById(R.id.settingsProfileMenuTextView)).setTypeface(font);
    }

    private void updateViewsData() {
        languageTextView.setText(user.getString(Constants.PARSE.LANGUAGE));
        userNameTextView.setText(String.format("@%s", user.getUsername()));
        userEmailTextView.setText(user.getEmail());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.defaultLanguageTextView:
            case R.id.languageTextView:
                showLanguageDialog();
                break;
            case R.id.sendFeedbackTextView:
                sendEmailIntent();
                break;
            case R.id.inviteFriendsTextView:
                showInviteFriendsDialog();
                break;
            case R.id.privacyPolicyTextView:
                WebViewActivity.start(getApplicationContext());
                break;
            case R.id.signOutTextView:
                ParseUser.logOut();
                StartActivity.start(getApplicationContext());
                break;
            case R.id.facebookTextView:
                facebookAppInvites();
                break;
            case R.id.gmailTextView:
                googleAppInvites();
                break;
        }
    }

    private void showLanguageDialog() {
        languageDialog = new LanguageDialog(this);
        languageDialog.createLanguageDialog();
        languageDialog.show();
    }

    private void showInviteFriendsDialog() {
        inviteFriendDialog = new InviteFriendDialog(this);
        inviteFriendDialog.createInviteDialog();
        inviteFriendDialog.show();
    }

    //TODO change email address
    private void sendEmailIntent() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"asphalte.support@email.com"});
        try {
            startActivity(Intent.createChooser(i, getResources().getString(R.string.send_feedback_text) + "..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_email_client_text), Toast.LENGTH_SHORT).show();
        }
    }

    //TODO change content (title , message , image) & refactoring
    private void googleAppInvites() {
        Intent intent = new AppInviteInvitation.IntentBuilder("Asphalte")
                .setMessage("Check this app!")
                        //  .setDeepLink(Uri.parse("invitation_deep_link"))
                        //   .setCustomImage(Uri.parse(getString(R.string.invitation_custom_image)))
                .setCallToActionText("invitation_cta")
                .build();
        startActivityForResult(intent, REQUEST_INVITE);

        inviteFriendDialog.cancel();
    }


    //TODO add google play app link & preview image
    private void facebookAppInvites() {
        String appLinkUrl = "https://play.google.com/store/apps/details?id=com.vsco.cam";
        String previewImageUrl = "http://i.utdstc.com/icons/256/vsco-cam-android.png";

        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(this, content);

            inviteFriendDialog.cancel();
        }
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.btn_close);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView textView = ((TextView) mToolbar.findViewById(R.id.toolbarTitle));
        textView.setText(getResources().getString(R.string.settings));
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
