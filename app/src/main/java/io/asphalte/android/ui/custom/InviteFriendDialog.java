package io.asphalte.android.ui.custom;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import io.asphalte.android.R;

/**
 * Created by dmytro on 12/4/15. Project : Asphalte
 */
public class InviteFriendDialog extends AlertDialog {

    private Activity mActivity;
    private View.OnClickListener listener;

    public InviteFriendDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
        this.listener = (View.OnClickListener) activity;
    }

    public InviteFriendDialog createInviteDialog() {
        View view = getLayoutInflater().inflate(R.layout.dialog_invite_friends_view, null);
        TextView msgTextView = (TextView) view.findViewById(R.id.msgTextView);
        TextView facebookTextView = (TextView) view.findViewById(R.id.facebookTextView);
        TextView gmailTextView = (TextView) view.findViewById(R.id.gmailTextView);

        facebookTextView.setOnClickListener(listener);
        gmailTextView.setOnClickListener(listener);

        msgTextView.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "Roboto-Regular.ttf"));
        gmailTextView.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fontello_social.ttf"));
        facebookTextView.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fontello_social.ttf"));

        setView(view);
        setCancelable(true);
        return this;
    }
}