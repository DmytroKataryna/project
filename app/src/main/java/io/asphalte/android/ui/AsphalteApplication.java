package io.asphalte.android.ui;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

import io.asphalte.android.R;
import io.asphalte.android.models.Post;
import io.branch.referral.Branch;

/**
 * Created by dmytro on 11/19/15. Project : Asphalte
 */
public class AsphalteApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(Post.class);
        Parse.initialize(this, getResources().getString(R.string.parse_app_id),getResources().getString(R.string.parse_client_key));
        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseFacebookUtils.initialize(getApplicationContext());

        Branch.getAutoInstance(this);
    }

}
