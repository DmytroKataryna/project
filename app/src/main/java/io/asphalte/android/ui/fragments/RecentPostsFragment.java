package io.asphalte.android.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.parse.ParseQuery;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.adapters.RecentPostsAdapter;
import io.asphalte.android.models.Post;

import java.util.ArrayList;

/**
 * Created by dmytro on 11/18/15. Project : Asphalte
 */

public class RecentPostsFragment extends AbstractFragment {

    public static RecentPostsFragment createInstance() {
        return new RecentPostsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.isRecent = true;
        View view = super.onCreateView(inflater, container, savedInstanceState);
        emptyText = getResources().getString(R.string.empty_list_text);
        mAdapter = new RecentPostsAdapter(new ArrayList<Post>(), this);
        mListView.setAdapter(mAdapter);
        return view;
    }

    protected void makeParseRequest(int offset, int count) {
        ParseQuery<Post> query = ParseQuery.getQuery(Constants.PARSE.POST);
        query.whereEqualTo(Constants.PARSE.IS_PRIVATE, false);
        query.addDescendingOrder(Constants.PARSE.CREATED_AT);
        query.setLimit(count);
        query.setSkip(offset);
        if (isAuthorized())
            query.whereNotContainedIn(Constants.PARSE.OWNER_ID, (ArrayList<String>) user.get(Constants.PARSE.HIDDEN_USERS));
        query.findInBackground(this);
    }
}
