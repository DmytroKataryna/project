package io.asphalte.android.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import io.asphalte.android.R;

/**
 * Created by dmytro on 12/3/15. Project : Asphalte
 */
public class WebViewActivity extends AppCompatActivity {

    private WebView mWebView;
    private ProgressBar progressBar;

    public static void start(Context context_) {
        context_.startActivity(new Intent(context_, WebViewActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        initToolbar();
        initViews();
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.btn_close);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView textView = ((TextView) mToolbar.findViewById(R.id.toolbarTitle));
        textView.setText("WebView Activity");
        textView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initViews() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.loadUrl("file:///android_asset/privacypolicy.html");
    }

}
