package io.asphalte.android.ui.post;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.SaveCallback;

import org.json.JSONObject;

import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.models.Post;
import io.asphalte.android.ui.profile.UserProfile;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;

/**
 * Created by dmytro on 11/26/15. Project : Asphalte
 */
public class PostDetailsActivity extends AbstractPostDetails implements View.OnClickListener {

    private TextView postTextView, userNameTextView;
    private FloatingActionButton fab;

    private Button followButton;
    private boolean isFollowing;
    private boolean isSaved;

    private ImageView splash;
    private LinearLayout postLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);

        String postID = getIntent().getStringExtra(Constants.PARSE.OBJECT_ID);
        String postOwnerUsername = getIntent().getStringExtra(Constants.PARSE.NAME);

        checkDeepLinking();
        initToolbar();
        initViews();
        changSplashViewVisibility(postID != null);
        loadPost(postID);
        initToolbarTitle(postOwnerUsername);
        preFetchDeepLink(postID);
    }

    private void changSplashViewVisibility(boolean status) {
        if (status) {
            splash.setVisibility(View.GONE);
            postLayout.setVisibility(View.VISIBLE);
        }
    }

    private void initViews() {
        Typeface robotoFont = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        postTextView = (TextView) findViewById(R.id.postTextView);
        dateTextView = (TextView) findViewById(R.id.dateTextView);
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        userNameTextView = (TextView) findViewById(R.id.userNameTextView);

        followButton = (Button) findViewById(R.id.followButton);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        titleTextView.setTypeface(Typeface.SERIF, Typeface.BOLD);
        postTextView.setTypeface(Typeface.SERIF);
        followButton.setTypeface(robotoFont);
        userNameTextView.setTypeface(robotoFont);

        followButton.setOnClickListener(this);
        userNameTextView.setOnClickListener(this);
        fab.setOnClickListener(this);

        followButton.setTransformationMethod(null);

        splash = (ImageView) findViewById(R.id.splash_image_view);
        postLayout = (LinearLayout) findViewById(R.id.post_details_layout);
    }

    private void checkDeepLinking() {
        Branch branch = Branch.getInstance();
        branch.initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    try {
                        String postID = referringParams.getString(Constants.BRANCH.POST_ID_BRANCH);
                        loadPost(postID);
                    } catch (Exception ignored) {
                    }
                }
            }
        }, this.getIntent().getData(), this);
    }

    protected void loadPost(String currentPostID) {
        if (currentPostID == null) return;
        Post.loadPost(currentPostID, new Post.GetPost() {
            @Override
            public void done(Post object, ParseException e) {
                if (e == null) {
                    if (user.checkIfSelfPost(object))
                        redirectToOwnPost(object);
                    else
                        updatePostViews(object);
                }
            }
        });
    }

    private void redirectToOwnPost(Post post) {
        startActivity(new Intent(getApplicationContext(), OwnPostDetailsActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(Constants.PARSE.OBJECT_ID, post.getObjectId()));
        finish();
    }

    private void updatePostViews(Post object) {
        post = object;
        changSplashViewVisibility(true);
        initToolbarTitle(post.getOwnerUsername());
        setPostDate();
        setPostTitle();
        postTextView.setText(post.getText());
        userNameTextView.setText(String.format("@%s", post.getOwnerUsername()));
        followButton.setVisibility(View.VISIBLE);

        checkIfFollowed();
        changeFollowButtonView(isFollowing);

        checkIfSaved();
        changeSavedButtonView(isSaved);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.userNameTextView:
                if (!user.checkIfSelfPost(post))
                    startActivity(new Intent(getApplicationContext(), UserProfile.class)
                            .putExtra(Constants.PARSE.OBJECT_ID, post.getOwnerID())
                            .putExtra(Constants.PARSE.NAME, post.getOwnerUsername())
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case R.id.fab:
                if (checkIfAuthorized()) {
                    saveButtonEnable(false);
                    saveButtonInteraction(isSaved);
                } else
                    showLoginDialog();
                break;
            case R.id.followButton:
                if (checkIfAuthorized()) {
                    followButtonEnable(false);
                    followButtonInteraction(isFollowing);
                } else
                    showLoginDialog();
                break;
            case R.id.menuTextViewToolbar:
                sendShareIntent();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_post_details, menu);
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.getItemId() == R.id.share) {
                View view = MenuItemCompat.getActionView(item).findViewById(R.id.menuTextViewToolbar);
                if (view != null) {
                    ((TextView) view).setText(getResources().getText(R.string.fontello_icon_share));
                    ((TextView) view).setTypeface(Typeface.createFromAsset(getAssets(), "fontello_social.ttf"));
                    view.setOnClickListener(this);
                }
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    protected void initToolbar() {
        super.initToolbar();
        mToolbar.setNavigationIcon(R.drawable.btn_close);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbarTextView = ((TextView) mToolbar.findViewById(R.id.toolbarTitle));
        toolbarTextView.setTypeface(Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf"));
    }

    private void checkIfSaved() {
        isSaved = user.checkIfPostSaved(post);
    }

    private void changeSavedButtonView(boolean isSaved) {
        if (isSaved)
            fab.setImageResource(R.drawable.bttn_floating_saved);
        else
            fab.setImageResource(R.drawable.bttn_floating_save);
    }

    private void saveButtonInteraction(boolean isSaved) {
        checkIfSaved();
        user.savePost(post, isSaved, new SavePostCallback());
    }

    private void changeSaveButtonState() {
        isSaved = !isSaved;
        changeSavedButtonView(isSaved);
    }

    private void checkIfFollowed() {
        isFollowing = user.checkIfFollowed(post);
    }

    private void followButtonInteraction(boolean isFollowing) {
        checkIfFollowed();
        user.subscribeUser(post.getOwnerID(), isFollowing, new SubscribeUserCallback());
    }

    private void changeFollowButtonState() {
        isFollowing = !isFollowing;
        changeFollowButtonView(isFollowing);
    }

    private void changeFollowButtonView(boolean isFollowing) {
        if (isFollowing) {
            followButton.setBackgroundResource(R.drawable.button_black_background);
            followButton.setTextColor(getResources().getColor(R.color.white));
            followButton.setText(getResources().getString(R.string.following_text));
        } else {
            followButton.setBackgroundResource(R.drawable.button_background_with_black_frame);
            followButton.setTextColor(getResources().getColor(R.color.black));
            followButton.setText(getResources().getString(R.string.follow_text));
        }
    }

    private void followButtonEnable(boolean enable) {
        followButton.setEnabled(enable);
    }

    private void saveButtonEnable(boolean enable) {
        fab.setEnabled(enable);
    }

    public class SavePostCallback implements SaveCallback {

        @Override
        public void done(ParseException e) {
            if (e == null) {
                user.sendPushNotification(post, isSaved);
                changeSaveButtonState();
            }
            saveButtonEnable(true);
        }
    }

    public class SubscribeUserCallback implements SaveCallback {

        @Override
        public void done(ParseException e) {
            if (e == null)
                changeFollowButtonState();

            followButtonEnable(true);
        }
    }
}
