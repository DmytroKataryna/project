package io.asphalte.android.ui.custom;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import io.asphalte.android.R;

/**
 * Created by dmytro on 11/23/15. Project : Asphalte
 */
public class LoginDialog extends AlertDialog {

    private Context mActivity;
    private View.OnClickListener listener;

    public LoginDialog(Activity activity) {
        super(activity);
        this.mActivity = activity;
        this.listener = (View.OnClickListener) activity;
    }

    public LoginDialog(Context context, View.OnClickListener listener) {
        super(context);
        this.mActivity = context;
        this.listener = listener;
    }

    public AlertDialog createLoginDialog() {
        View title = getLayoutInflater().inflate(R.layout.dialog_login_title_view, null);
        View view = getLayoutInflater().inflate(R.layout.dialog_login_msg_view, null);
        view.findViewById(R.id.loginDialogNegativeButton).setOnClickListener(listener);
        view.findViewById(R.id.loginDialogPositiveButton).setOnClickListener(listener);

        TextView titleTxt = (TextView) title.findViewById(R.id.titleTextView);
        titleTxt.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "Roboto-Regular.ttf"));

        setCustomTitle(title);
        setView(view);
        setCancelable(false);
        return this;
    }
}
