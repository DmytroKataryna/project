package io.asphalte.android;

/**
 * Created by dmytro on 11/19/15. Project : Asphalte
 */
public class Constants {

    public static final String APP_TAG = "asphalte_tag";
    public static final String EMPTY = "";

    public static final int ZERO = 0;
    public static final int LIST_ITEMS_TO_LOAD = 20;

    public static class PARSE {
        public final static String POST = "Post";
        public final static String OBJECT_ID = "objectId";
        public final static String PUSH_POST_ID = "postID";
        public final static String PUSH_USER_ID = "userID";
        public static final String KEY_PUSH_DATA = "com.parse.Data";

        public final static String LANGUAGE = "language";
        public final static String LANGUAGE_ID = "languageID";
        public final static String EMAIL = "email";
        public final static String NAME = "name";
        public final static String HIDDEN_USERS = "hidden_users";
        public final static String SUBSCRIBED_USERS = "subscribed_users";

        public final static String SAVED_IDS = "saved_ids";
        public final static String TITLE = "title";
        public final static String TEXT = "text";
        public final static String IS_PRIVATE = "is_private";
        public final static String OWNER_ID = "ownerID";
        public final static String OWNER_USERNAME = "ownerUsername";
        public final static String CREATED_AT = "createdAt";
        public static final String SUBSCRIBERS = "subscribers";

        public final static String REPORT = "Report";
        public final static String FROM = "from";
        public final static String TO = "to";
        public final static String MSG = "msg";
    }

    public static class BRANCH {
        public static final String POST_ID_BRANCH = "asphaltePostID";
    }
}
