package io.asphalte.android.helpers;

import android.content.Context;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.parse.ParseException;
import com.parse.ParseUser;
import io.asphalte.android.Constants;
import io.asphalte.android.ui.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by dmytro on 11/20/15. Project : Asphalte
 */
public class FacebookHelper {

    public static void signUpByFacebook(final Context context) {
        final SaveCallback callback = new SaveCallback(context);
        final GraphCallback graphCallback = new GraphCallback(callback);
        final GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), graphCallback);

        Bundle parameters = new Bundle();
        parameters.putString("fields", Constants.PARSE.EMAIL + "," + Constants.PARSE.NAME);
        request.setParameters(parameters);
        request.executeAsync();
    }

    private static class GraphCallback implements GraphRequest.GraphJSONObjectCallback {

        private com.parse.SaveCallback callback;

        public GraphCallback(com.parse.SaveCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onCompleted(JSONObject object, GraphResponse response) {
            ParseUser user = ParseUser.getCurrentUser();
            try {
                user.setEmail(object.getString(Constants.PARSE.EMAIL));
                user.setUsername(object.getString(Constants.PARSE.NAME));
                user.put(Constants.PARSE.LANGUAGE, Locale.getDefault().getLanguage());
                user.addAll(Constants.PARSE.HIDDEN_USERS, new ArrayList<Integer>());
                user.addAll(Constants.PARSE.SUBSCRIBED_USERS, new ArrayList<Integer>());
                user.addAll(Constants.PARSE.SAVED_IDS, new ArrayList<Integer>());
                user.saveInBackground(callback);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static class SaveCallback implements com.parse.SaveCallback {

        private Context context;

        public SaveCallback(Context context) {
            this.context = context;
        }

        @Override
        public void done(ParseException e) {
            MainActivity.start(context);
        }
    }
}
