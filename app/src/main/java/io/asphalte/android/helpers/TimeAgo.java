package io.asphalte.android.helpers;

import android.content.Context;
import android.content.res.Resources;

import io.asphalte.android.R;

import java.util.Date;

public class TimeAgo {


    public static String toDuration(Context context, Date date) {
        return toDuration(context, date.getTime());
    }

    public static String toDuration(Context context, long millis) {
        long diff = new Date().getTime() - millis;
        boolean justNow = false;

        Resources r = context.getResources();

        String suffix = r.getString(R.string.time_ago_suffix);

        double seconds = Math.abs(diff) / 1000;
        double minutes = seconds / 60;
        double hours = minutes / 60;
        double days = hours / 24;
        double years = days / 365;

        String words;

        if (seconds < 45) {
            words = r.getString(R.string.time_ago_seconds, Math.round(seconds));
            justNow = true;
        } else if (seconds < 90) {
            words = r.getString(R.string.time_ago_minute, 1);
        } else if (minutes < 45) {
            words = r.getString(R.string.time_ago_minutes, Math.round(minutes));
        } else if (minutes < 90) {
            words = r.getString(R.string.time_ago_hour, 1);
        } else if (hours < 24) {
            words = r.getString(R.string.time_ago_hours, Math.round(hours));
        } else if (hours < 42) {
            words = r.getString(R.string.time_ago_day, 1);
        } else if (days < 30) {
            words = r.getString(R.string.time_ago_days, Math.round(days));
        } else if (days < 45) {
            words = r.getString(R.string.time_ago_month, 1);
        } else if (days < 365) {
            words = r.getString(R.string.time_ago_months, Math.round(days / 30));
        } else if (years < 1.5) {
            words = r.getString(R.string.time_ago_year, 1);
        } else {
            words = r.getString(R.string.time_ago_years, Math.round(years));
        }

        StringBuilder sb = new StringBuilder();
        sb.append(words);

        if (suffix.length() > 0 && !justNow) {
            sb.append(" ").append(suffix);
        }

        return sb.toString().trim();
    }
}