package io.asphalte.android.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import io.asphalte.android.Constants;

/**
 * Created by dmytro on 11/19/15. Project : Asphalte
 */
public class PreferencesUtils {

    private static PreferencesUtils sUtils;
    private static SharedPreferences sharedPref;

    // *****************  preferences data *****************
    private static final String KEY_SHARED_PREF = "ANDROID_ASPHALTE";
    private static final int KEY_MODE_PRIVATE = Constants.ZERO;

    //************** session user data **************

    public static final String CURRENT_USER_ID = "sessionUserID";

    //************** tab data **************
    private static final String SELECTED_TAB_ID = "selectedTabID";

    //****************************  PREFERENCES ************************************

    public PreferencesUtils(Context context) {
        sharedPref = context.getSharedPreferences(KEY_SHARED_PREF, KEY_MODE_PRIVATE);
    }

    public static PreferencesUtils get(Context c) {
        if (sUtils == null) {
            sUtils = new PreferencesUtils(c.getApplicationContext());
        }
        return sUtils;
    }

    public void setSelectedTabID(int tabID) {
        sharedPref.edit().putInt(SELECTED_TAB_ID, tabID).apply();
    }

    public int getSelectedTabID() {
        return sharedPref.getInt(SELECTED_TAB_ID, Constants.ZERO);
    }

    public void setPushNotificationID(String key, int postID) {
        sharedPref.edit().putInt(key, postID).apply();
    }

    public int getPushNotificationID(String key) {
        return sharedPref.getInt(key, -1);
    }


}
