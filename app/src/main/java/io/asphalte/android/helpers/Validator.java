package io.asphalte.android.helpers;

import java.util.regex.Pattern;

/**
 * Created by dmytro on 12/8/15. Project : Asphalte
 */
public class Validator {

    private static int MIN_LENGTH = 3;

    private static String emailRegex = "^[A-Za-z0-9+_.-]+@(.+)$";
    private static String userNameRegex = "^([A-Z]|[a-z])+( ([A-Z]|[a-z])+)?$";

    public static final String
            NAME = "name",
            EMAIL = "email",
            PASSWORD = "password";


    public static String validate(String field, String type) {
        switch (type) {
            case NAME:
                return validateName(field);
            case EMAIL:
                return validateEmail(field);
            case PASSWORD:
                return validatePassword(field);
        }
        return null;
    }

    public static String validateName(String name) {
        if (!checkLength(name))
            return "Sorry, name must contain at least 3 characters.";
        else if (!checkFieldByRegex(name, userNameRegex))
            return "Incorrect name";

        return null;
    }

    public static String validateEmail(String email) {
        if (!checkLength(email))
            return "Sorry, email must contain at least 3 characters.";
        else if (!checkFieldByRegex(email, emailRegex))
            return "Incorrect email";

        return null;
    }

    public static String validatePassword(String password) {
        if (!checkLength(password))
            return "Sorry, password must contain at least 3 characters.";

        return null;
    }

    private static boolean checkLength(String field) {
        return field.length() > MIN_LENGTH;
    }

    private static boolean checkFieldByRegex(String field, String regex) {
        return Pattern.matches(regex, field);
    }

}
