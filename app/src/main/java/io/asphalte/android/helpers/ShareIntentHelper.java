package io.asphalte.android.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Parcelable;

import io.asphalte.android.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dmytro on 12/9/15. Project : Asphalte
 */
public class ShareIntentHelper {

    private static final String PACKAGE_NAME = "packageName";
    private static final String SIMPLE_NAME = "simpleName";
    private static final String CLASS_NAME = "className";

    private static String[] forbiddenChoices = new String[]{
            "com.google.android.talk",
            "com.google.android.apps.docs",
            "com.google.android.apps.plus",
            "com.google.android.gm",
            "com.facebook.katana",
            "com.twitter.android",
            "com.vkontakte.android"};


    public static Intent generateCustomChooserIntent(Context context, Intent prototype) {
        List<Intent> targetedShareIntents = new ArrayList<>();
        List<HashMap<String, String>> intentMetaInfo = new ArrayList<>();
        Intent chooserIntent;

        Intent intent = new Intent(prototype.getAction());
        intent.setType(prototype.getType());
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);

        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (!(resolveInfo.activityInfo == null || Arrays.asList(forbiddenChoices).contains(resolveInfo.activityInfo.packageName)))
                    continue;

                HashMap<String, String> info = new HashMap<>();
                info.put(PACKAGE_NAME, resolveInfo.activityInfo.packageName);
                info.put(CLASS_NAME, resolveInfo.activityInfo.name);
                info.put(SIMPLE_NAME, String.valueOf(resolveInfo.activityInfo.loadLabel(context.getPackageManager())));
                intentMetaInfo.add(info);
            }

            if (!intentMetaInfo.isEmpty()) {
                // sorting for nice readability
                Collections.sort(intentMetaInfo, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> map, HashMap<String, String> map2) {
                        return map.get(SIMPLE_NAME).compareTo(map2.get(SIMPLE_NAME));
                    }
                });

                // create the custom intent list
                for (HashMap<String, String> metaInfo : intentMetaInfo) {
                    Intent targetedShareIntent = (Intent) prototype.clone();
                    targetedShareIntent.setPackage(metaInfo.get(PACKAGE_NAME));
                    targetedShareIntent.setClassName(metaInfo.get(PACKAGE_NAME), metaInfo.get(CLASS_NAME));
                    targetedShareIntents.add(targetedShareIntent);
                }

                chooserIntent = Intent.createChooser(targetedShareIntents.remove(targetedShareIntents.size() - 1), context.getResources().getString(R.string.share_dialog_text));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));
                return chooserIntent;
            }
        }

        return Intent.createChooser(prototype, context.getResources().getString(R.string.share_dialog_text));
    }
}
