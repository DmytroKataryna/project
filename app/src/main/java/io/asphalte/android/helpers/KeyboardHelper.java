package io.asphalte.android.helpers;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by dmytro on 9/4/15.
 */
public class KeyboardHelper implements ViewTreeObserver.OnGlobalLayoutListener {

    public interface KeyboardListener {
        void onKeyboardVisible(boolean visible);
    }

    private int mHeight;
    private boolean mVisible;
    private View mRootView;
    private KeyboardListener mListener;

    public KeyboardHelper(Activity activity) {
        mVisible = false;
        mHeight = 0;
        if (activity == null) {
            return;
        }

        try {
            mRootView = activity.getWindow().getDecorView()
                    .findViewById(android.R.id.content);
            mRootView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setListener(KeyboardListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onGlobalLayout() {
        if (mHeight == 0) {
            mHeight = mRootView.getMeasuredHeight();
            return;
        }

        if (mListener == null) {
            return;
        }

        int height = mRootView.getHeight();

        if (!mVisible && mHeight > (height + 100)) {
            mVisible = true;
            mListener.onKeyboardVisible(mVisible);
            mHeight = height;
        } else if (mVisible && mHeight < (height - 100)) {
            mVisible = false;
            mListener.onKeyboardVisible(mVisible);
            mHeight = height;
        }
    }

    public static void hide(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getApplicationWindowToken(), 0);
        } catch (NullPointerException ignored) {

        }
    }

    public static void show(Activity activity) {
        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (NullPointerException ignored) {

        }
    }
}