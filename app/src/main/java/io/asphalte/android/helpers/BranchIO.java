package io.asphalte.android.helpers;

import android.content.Context;

import io.asphalte.android.Constants;
import io.asphalte.android.models.Post;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.util.LinkProperties;

/**
 * Created by dmytro on 12/15/15. Project : Asphalte
 */
public class BranchIO {

    public static void generateShortUrl(Context context, String postID, Branch.BranchLinkCreateListener callback) {
        BranchUniversalObject branchUniversalObject = new BranchUniversalObject()
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .addContentMetadata(Constants.BRANCH.POST_ID_BRANCH, postID);

        LinkProperties linkProperties = new LinkProperties();

        branchUniversalObject.generateShortUrl(context, linkProperties, callback);
    }
}
