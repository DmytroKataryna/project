package io.asphalte.android.helpers;

import java.util.ArrayList;


/**
 * Created by dmytro on 12/4/15. Project : Asphalte
 */
public class LanguageUtils {

    private static ArrayList<AsphalteLocale> localeMap;

    public static ArrayList<AsphalteLocale> getUniqueLocale() {
        if (localeMap == null)
            initializeMap();
        return localeMap;
    }

    private static void initializeMap() {
        localeMap = new ArrayList<>();
        localeMap.add(new AsphalteLocale("afr", "Afrikaans"));
        localeMap.add(new AsphalteLocale("agq", "Aghem"));
        localeMap.add(new AsphalteLocale("aka", "Akan"));
        localeMap.add(new AsphalteLocale("amh", "Amharic"));
        localeMap.add(new AsphalteLocale("ara", "Arabic"));
        localeMap.add(new AsphalteLocale("asm", "Assamese"));
        localeMap.add(new AsphalteLocale("asa", "Asu"));
        localeMap.add(new AsphalteLocale("aze", "Azerbaijani"));
        localeMap.add(new AsphalteLocale("bas", "Basaa"));
        localeMap.add(new AsphalteLocale("bel", "Belarusian"));
        localeMap.add(new AsphalteLocale("bem", "Bemba"));
        localeMap.add(new AsphalteLocale("bez", "Bena"));
        localeMap.add(new AsphalteLocale("bul", "Bulgarian"));
        localeMap.add(new AsphalteLocale("bam", "Bambara"));
        localeMap.add(new AsphalteLocale("ben", "Bengali"));
        localeMap.add(new AsphalteLocale("bod", "Tibetan"));
        localeMap.add(new AsphalteLocale("bre", "Breton"));
        localeMap.add(new AsphalteLocale("brx", "Bodo"));
        localeMap.add(new AsphalteLocale("bos", "Bosnian"));
        localeMap.add(new AsphalteLocale("cat", "Catalan"));
        localeMap.add(new AsphalteLocale("cgg", "Chiga"));
        localeMap.add(new AsphalteLocale("chr", "Cherokee"));
        localeMap.add(new AsphalteLocale("ces", "Czech"));
        localeMap.add(new AsphalteLocale("cym", "Welsh"));
        localeMap.add(new AsphalteLocale("dan", "Danish"));
        localeMap.add(new AsphalteLocale("dav", "Taita"));
        localeMap.add(new AsphalteLocale("deu", "German"));
        localeMap.add(new AsphalteLocale("dje", "Zarma"));
        localeMap.add(new AsphalteLocale("dsb", "Lower Sorbian"));
        localeMap.add(new AsphalteLocale("dua", "Duala"));
        localeMap.add(new AsphalteLocale("dyo", "Jola-Fonyi"));
        localeMap.add(new AsphalteLocale("dzo", "Dzongkha"));
        localeMap.add(new AsphalteLocale("ebu", "Embu"));
        localeMap.add(new AsphalteLocale("ewe", "Ewe"));
        localeMap.add(new AsphalteLocale("ell", "Greek"));
        localeMap.add(new AsphalteLocale("eng", "English"));
        localeMap.add(new AsphalteLocale("epo", "Esperanto"));
        localeMap.add(new AsphalteLocale("spa", "Spanish"));
        localeMap.add(new AsphalteLocale("est", "Estonian"));
        localeMap.add(new AsphalteLocale("eus", "Basque"));
        localeMap.add(new AsphalteLocale("ewo", "Ewondo"));
        localeMap.add(new AsphalteLocale("fas", "Persian"));
        localeMap.add(new AsphalteLocale("ful", "Fulah"));
        localeMap.add(new AsphalteLocale("fin", "Finnish"));
        localeMap.add(new AsphalteLocale("fil", "Filipino"));
        localeMap.add(new AsphalteLocale("fao", "Faroese"));
        localeMap.add(new AsphalteLocale("fra", "French"));
        localeMap.add(new AsphalteLocale("fur", "Friulian"));
        localeMap.add(new AsphalteLocale("fry", "Western Frisian"));
        localeMap.add(new AsphalteLocale("gle", "Irish"));
        localeMap.add(new AsphalteLocale("gla", "Scottish Gaelic"));
        localeMap.add(new AsphalteLocale("glg", "Galician"));
        localeMap.add(new AsphalteLocale("gsw", "Swiss German"));
        localeMap.add(new AsphalteLocale("guj", "Gujarati"));
        localeMap.add(new AsphalteLocale("guz", "Gusii"));
        localeMap.add(new AsphalteLocale("glv", "Manx"));
        localeMap.add(new AsphalteLocale("hau", "Hausa"));
        localeMap.add(new AsphalteLocale("haw", "Hawaiian"));
        localeMap.add(new AsphalteLocale("heb", "Hebrew"));
        localeMap.add(new AsphalteLocale("hin", "Hindi"));
        localeMap.add(new AsphalteLocale("hrv", "Croatian"));
        localeMap.add(new AsphalteLocale("hsb", "Upper Sorbian"));
        localeMap.add(new AsphalteLocale("hun", "Hungarian"));
        localeMap.add(new AsphalteLocale("hye", "Armenian"));
        localeMap.add(new AsphalteLocale("ind", "Indonesian"));
        localeMap.add(new AsphalteLocale("ibo", "Igbo"));
        localeMap.add(new AsphalteLocale("iii", "Sichuan Yi"));
        localeMap.add(new AsphalteLocale("isl", "Icelandic"));
        localeMap.add(new AsphalteLocale("ita", "Italian"));
        localeMap.add(new AsphalteLocale("jpn", "Japanese"));
        localeMap.add(new AsphalteLocale("jgo", "Ngomba"));
        localeMap.add(new AsphalteLocale("jmc", "Machame"));
        localeMap.add(new AsphalteLocale("kat", "Georgian"));
        localeMap.add(new AsphalteLocale("kab", "Kabyle"));
        localeMap.add(new AsphalteLocale("kam", "Kamba"));
        localeMap.add(new AsphalteLocale("kde", "Makonde"));
        localeMap.add(new AsphalteLocale("kea", "Kabuverdianu"));
        localeMap.add(new AsphalteLocale("khq", "Koyra Chiini"));
        localeMap.add(new AsphalteLocale("kik", "Kikuyu"));
        localeMap.add(new AsphalteLocale("kaz", "Kazakh"));
        localeMap.add(new AsphalteLocale("kkj", "Kako"));
        localeMap.add(new AsphalteLocale("kal", "Kalaallisut"));
        localeMap.add(new AsphalteLocale("kln", "Kalenjin"));
        localeMap.add(new AsphalteLocale("khm", "Khmer"));
        localeMap.add(new AsphalteLocale("kan", "Kannada"));
        localeMap.add(new AsphalteLocale("kor", "Korean"));
        localeMap.add(new AsphalteLocale("kok", "Konkani"));
        localeMap.add(new AsphalteLocale("kas", "Kashmiri"));
        localeMap.add(new AsphalteLocale("ksb", "Shambala"));
        localeMap.add(new AsphalteLocale("ksf", "Bafia"));
        localeMap.add(new AsphalteLocale("ksh", "Colognian"));
        localeMap.add(new AsphalteLocale("cor", "Cornish"));
        localeMap.add(new AsphalteLocale("kir", "Kyrgyz"));
        localeMap.add(new AsphalteLocale("lag", "Langi"));
        localeMap.add(new AsphalteLocale("ltz", "Luxembourgish"));
        localeMap.add(new AsphalteLocale("lug", "Ganda"));
        localeMap.add(new AsphalteLocale("lkt", "Lakota"));
        localeMap.add(new AsphalteLocale("lin", "Lingala"));
        localeMap.add(new AsphalteLocale("lao", "Lao"));
        localeMap.add(new AsphalteLocale("lit", "Lithuanian"));
        localeMap.add(new AsphalteLocale("lub", "Luba-Katanga"));
        localeMap.add(new AsphalteLocale("luo", "Luo"));
        localeMap.add(new AsphalteLocale("luy", "Luyia"));
        localeMap.add(new AsphalteLocale("lav", "Latvian"));
        localeMap.add(new AsphalteLocale("mas", "Masai"));
        localeMap.add(new AsphalteLocale("mer", "Meru"));
        localeMap.add(new AsphalteLocale("mfe", "Morisyen"));
        localeMap.add(new AsphalteLocale("mlg", "Malagasy"));
        localeMap.add(new AsphalteLocale("mgh", "Makhuwa-Meetto"));
        localeMap.add(new AsphalteLocale("mgo", "Metaʼ"));
        localeMap.add(new AsphalteLocale("mkd", "Macedonian"));
        localeMap.add(new AsphalteLocale("mal", "Malayalam"));
        localeMap.add(new AsphalteLocale("mon", "Mongolian"));
        localeMap.add(new AsphalteLocale("mar", "Marathi"));
        localeMap.add(new AsphalteLocale("msa", "Malay"));
        localeMap.add(new AsphalteLocale("mlt", "Maltese"));
        localeMap.add(new AsphalteLocale("mua", "Mundang"));
        localeMap.add(new AsphalteLocale("mya", "Burmese"));
        localeMap.add(new AsphalteLocale("naq", "Nama"));
        localeMap.add(new AsphalteLocale("nob", "Norwegian Bokmål"));
        localeMap.add(new AsphalteLocale("nde", "North Ndebele"));
        localeMap.add(new AsphalteLocale("nep", "Nepali"));
        localeMap.add(new AsphalteLocale("nld", "Dutch"));
        localeMap.add(new AsphalteLocale("nmg", "Kwasio"));
        localeMap.add(new AsphalteLocale("nno", "Norwegian Nynorsk"));
        localeMap.add(new AsphalteLocale("nnh", "Ngiemboon"));
        localeMap.add(new AsphalteLocale("nus", "Nuer"));
        localeMap.add(new AsphalteLocale("nyn", "Nyankole"));
        localeMap.add(new AsphalteLocale("orm", "Oromo"));
        localeMap.add(new AsphalteLocale("ori", "Oriya"));
        localeMap.add(new AsphalteLocale("oss", "Ossetic"));
        localeMap.add(new AsphalteLocale("pan", "Punjabi"));
        localeMap.add(new AsphalteLocale("pol", "Polish"));
        localeMap.add(new AsphalteLocale("pus", "Pashto"));
        localeMap.add(new AsphalteLocale("por", "Portuguese"));
        localeMap.add(new AsphalteLocale("que", "Quechua"));
        localeMap.add(new AsphalteLocale("roh", "Romansh"));
        localeMap.add(new AsphalteLocale("run", "Rundi"));
        localeMap.add(new AsphalteLocale("ron", "Romanian"));
        localeMap.add(new AsphalteLocale("rof", "Rombo"));
        localeMap.add(new AsphalteLocale("rus", "Russian"));
        localeMap.add(new AsphalteLocale("kin", "Kinyarwanda"));
        localeMap.add(new AsphalteLocale("rwk", "Rwa"));
        localeMap.add(new AsphalteLocale("sah", "Sakha"));
        localeMap.add(new AsphalteLocale("saq", "Samburu"));
        localeMap.add(new AsphalteLocale("sbp", "Sangu"));
        localeMap.add(new AsphalteLocale("sme", "Northern Sami"));
        localeMap.add(new AsphalteLocale("seh", "Sena"));
        localeMap.add(new AsphalteLocale("ses", "Koyraboro Senni"));
        localeMap.add(new AsphalteLocale("sag", "Sango"));
        localeMap.add(new AsphalteLocale("shi", "Tachelhit"));
        localeMap.add(new AsphalteLocale("sin", "Sinhala"));
        localeMap.add(new AsphalteLocale("slk", "Slovak"));
        localeMap.add(new AsphalteLocale("slv", "Slovenian"));
        localeMap.add(new AsphalteLocale("smn", "Inari Sami"));
        localeMap.add(new AsphalteLocale("sna", "Shona"));
        localeMap.add(new AsphalteLocale("som", "Somali"));
        localeMap.add(new AsphalteLocale("sqi", "Albanian"));
        localeMap.add(new AsphalteLocale("srp", "Serbian"));
        localeMap.add(new AsphalteLocale("swe", "Swedish"));
        localeMap.add(new AsphalteLocale("swa", "Swahili"));
        localeMap.add(new AsphalteLocale("tam", "Tamil"));
        localeMap.add(new AsphalteLocale("tel", "Telugu"));
        localeMap.add(new AsphalteLocale("teo", "Teso"));
        localeMap.add(new AsphalteLocale("tha", "Thai"));
        localeMap.add(new AsphalteLocale("tir", "Tigrinya"));
        localeMap.add(new AsphalteLocale("ton", "Tongan"));
        localeMap.add(new AsphalteLocale("tur", "Turkish"));
        localeMap.add(new AsphalteLocale("twq", "Tasawaq"));
        localeMap.add(new AsphalteLocale("tzm", "Central Atlas Tamazight"));
        localeMap.add(new AsphalteLocale("uig", "Uyghur"));
        localeMap.add(new AsphalteLocale("ukr", "Ukrainian"));
        localeMap.add(new AsphalteLocale("urd", "Urdu"));
        localeMap.add(new AsphalteLocale("uzb", "Uzbek"));
        localeMap.add(new AsphalteLocale("vai", "Vai"));
        localeMap.add(new AsphalteLocale("vie", "Vietnamese"));
        localeMap.add(new AsphalteLocale("vun", "Vunjo"));
        localeMap.add(new AsphalteLocale("wae", "Walser"));
        localeMap.add(new AsphalteLocale("xog", "Soga"));
        localeMap.add(new AsphalteLocale("yav", "Yangben"));
        localeMap.add(new AsphalteLocale("yid", "Yiddish"));
        localeMap.add(new AsphalteLocale("yor", "Yoruba"));
        localeMap.add(new AsphalteLocale("zgh", "Standard Moroccan Tamazight"));
        localeMap.add(new AsphalteLocale("zho", "Chinese"));
        localeMap.add(new AsphalteLocale("zul", "Zulu"));
    }

    public static class AsphalteLocale {

        private String languageName;
        private String language;

        public AsphalteLocale(String language, String languageName) {
            this.languageName = languageName;
            this.language = language;
        }

        public String getLanguageName() {
            return languageName;
        }

        public void setLanguageName(String languageName) {
            this.languageName = languageName;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }
    }
}
