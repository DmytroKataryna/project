package io.asphalte.android.models;

import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import io.asphalte.android.Constants;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dmytro on 11/20/15. Project : Asphalte
 */
@ParseClassName(Constants.PARSE.POST)
public class Post extends ParseObject implements Serializable {

    public Post() {
    }

    public String getText() {
        return getString(Constants.PARSE.TEXT);
    }

    public void setText(String text) {
        put(Constants.PARSE.TEXT, text);
    }

    public String getTitle() {
        return getString(Constants.PARSE.TITLE);
    }

    public void setTitle(String title) {
        if (title != null && title.length() > Constants.ZERO)
            put(Constants.PARSE.TITLE, title);
    }

    public boolean isPrivate() {
        return getBoolean(Constants.PARSE.IS_PRIVATE);
    }

    public void setPrivate(boolean is_private) {
        put(Constants.PARSE.IS_PRIVATE, is_private);
    }

    public void setOwner(ParseUser owner) {
        put(Constants.PARSE.OWNER_USERNAME, owner.getUsername());
        put(Constants.PARSE.OWNER_ID, owner.getObjectId());
    }

    public String getOwnerUsername() {
        return getString(Constants.PARSE.OWNER_USERNAME);
    }

    public String getOwnerID() {
        return getString(Constants.PARSE.OWNER_ID);
    }

    public ArrayList<String> getSubscribers() {
        return (ArrayList<String>) get(Constants.PARSE.SUBSCRIBERS);
    }

    public static void loadPost(String postID, GetPost callback) {
        ParseQuery<Post> query = ParseQuery.getQuery(Constants.PARSE.POST);
        query.getInBackground(postID, callback);
    }

    public interface GetPost extends GetCallback<Post> {
        void done(Post object, ParseException e);
    }
}
