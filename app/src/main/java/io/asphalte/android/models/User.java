package io.asphalte.android.models;

import com.parse.CountCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import io.asphalte.android.Constants;

import java.util.ArrayList;

/**
 * Created by dmytro on 12/7/15. Project : Asphalte
 */
public class User {

    private ParseUser user;

    public User(ParseUser currentUser) {
        this.user = currentUser;
    }

    public ArrayList<String> getSavedPosts() {
        return (ArrayList<String>) user.get(Constants.PARSE.SAVED_IDS);
    }

    public ArrayList<String> getSubscribedUsers() {
        return (ArrayList<String>) user.get(Constants.PARSE.SUBSCRIBED_USERS);
    }

    public ArrayList<String> getHiddenUsers() {
        return (ArrayList<String>) user.get(Constants.PARSE.HIDDEN_USERS);
    }

    public void savePost(Post post, boolean isSaved, SaveCallback callback) {
        ArrayList<String> userSavedPosts = getSavedPosts();
        if (isSaved)
            userSavedPosts.remove(post.getObjectId());
        else
            userSavedPosts.add(post.getObjectId());

        user.remove(Constants.PARSE.SAVED_IDS);
        user.addAll(Constants.PARSE.SAVED_IDS, userSavedPosts);
        user.saveInBackground(callback);
    }

    public void subscribeUser(String postOwnerID, boolean isFollowing, SaveCallback callback) {
        ArrayList<String> userSubscribers = getSubscribedUsers();

        if (isFollowing)
            userSubscribers.remove(postOwnerID);
        else
            userSubscribers.add(postOwnerID);

        user.remove(Constants.PARSE.SUBSCRIBED_USERS);
        user.addAll(Constants.PARSE.SUBSCRIBED_USERS, userSubscribers);
        user.saveInBackground(callback);
    }

    public void hideUserProfile(String userProfileID) {
        ArrayList<String> hiddenUser = getHiddenUsers();
        hiddenUser.add(userProfileID);
        user.remove(Constants.PARSE.HIDDEN_USERS);
        user.addAll(Constants.PARSE.HIDDEN_USERS, hiddenUser);
        user.saveInBackground();
    }

    public void countNumbersOfSavedPosts(String postID, CountCallback callback) {
        ParseQuery<ParseUser> queryUser = ParseUser.getQuery();
        queryUser.whereEqualTo(Constants.PARSE.SAVED_IDS, postID);
        queryUser.countInBackground(callback);
    }

    public boolean checkIfSelfPost(Post post) {
        return checkIfAuthorized() && user.getObjectId().equals(post.getOwnerID());
    }

    public boolean checkIfFollowed(Post post) {
        return checkIfAuthorized() && getSubscribedUsers().contains(post.getOwnerID());
    }

    public boolean checkIfPostSaved(Post post) {
        return checkIfAuthorized() && getSavedPosts().contains(post.getObjectId());
    }

    public boolean checkIfAuthorized() {
        return user != null;
    }

    public ParseUser getParseUser() {
        return user;
    }

    public void sendPushNotification(final Post post, boolean isPostSaved) {
        if (isPostSaved) return;
        ParseQuery<ParseInstallation> pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo(Constants.PARSE.PUSH_USER_ID, post.getOwnerID());
        final ParsePush push = new ParsePush();
        push.setQuery(pushQuery);

        countNumbersOfSavedPosts(post.getObjectId(), new CountCallback() {
            @Override
            public void done(int count, ParseException e) {
                push.setData(getPushData(post, "your post " + getPushText(post) + " has " + count + " saves"));
                push.sendInBackground();
            }
        });
    }

    private String getPushText(Post post) {
        if (post.getTitle() != null)
            return post.getTitle();
        else
            return (post.getText().length() > 10 ? post.getText().substring(0, 10) + "..." : post.getText());
    }

    private JSONObject getPushData(Post post, String messageText) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.PARSE.PUSH_USER_ID, post.getOwnerID());
            jsonObject.put(Constants.PARSE.PUSH_POST_ID, post.getObjectId());
            jsonObject.put("alert", messageText);
            jsonObject.put("title", "Asphalte");
            return jsonObject;
        } catch (JSONException e) {
            return null;
        }
    }
}
