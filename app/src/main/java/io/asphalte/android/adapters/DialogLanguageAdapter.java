package io.asphalte.android.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.parse.ParseUser;
import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.helpers.LanguageUtils;
import io.asphalte.android.ui.settings.SettingActivity;

import java.util.ArrayList;

/**
 * Created by dmytro on 12/3/15. Project : Asphalte
 */
public class DialogLanguageAdapter extends BaseAdapter implements View.OnClickListener {

    private Activity mActivity;
    private ListView mListView;
    private ParseUser user;
    private ArrayList<LanguageUtils.AsphalteLocale> data;

    public DialogLanguageAdapter(Activity activity, ListView listView) {
        this.mActivity = activity;
        this.mListView = listView;
        this.user = ParseUser.getCurrentUser();
        data = LanguageUtils.getUniqueLocale();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_language_list_row, parent, false);
            holder = new ViewHolder();
            holder.mDialogRowLayout = (LinearLayout) convertView.findViewById(R.id.dialogRowLayout);
            holder.mCategoryTextView = (TextView) convertView.findViewById(R.id.categoryTextView);
            holder.mRadioButtonView = (RadioButton) convertView.findViewById(R.id.radioButton);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final LanguageUtils.AsphalteLocale locale = data.get(position);
        holder.mCategoryTextView.setText(locale.getLanguageName());
        holder.mRadioButtonView.setChecked(false);

        if ((locale.getLanguage().equals(user.getString(Constants.PARSE.LANGUAGE_ID))))
            holder.mRadioButtonView.setChecked(true);

        holder.mRadioButtonView.setTag(locale);
        holder.mDialogRowLayout.setTag(locale);
        holder.mDialogRowLayout.setTag(R.id.TAG_LANGUAGE_DIALOG_CHECKBOX_ID, holder.mRadioButtonView);
        holder.mDialogRowLayout.setOnClickListener(this);
        holder.mRadioButtonView.setOnClickListener(this);

        return convertView;
    }

    @Override
    public void onClick(View v) {
        final LanguageUtils.AsphalteLocale locale = (LanguageUtils.AsphalteLocale) v.getTag();
        unCheckPreviousViews();

        RadioButton radioButton = retrieveRadioButton(v);
        radioButton.setChecked(true);

        user.put(Constants.PARSE.LANGUAGE, locale.getLanguageName());
        user.put(Constants.PARSE.LANGUAGE_ID, locale.getLanguage());
        user.saveInBackground();

        v.postDelayed(new Runnable() {
            @Override
            public void run() {
                ((SettingActivity) mActivity).languageTextView.setText(locale.getLanguageName());
                ((SettingActivity) mActivity).languageDialog.cancel();
            }
        }, 250);
    }

    private RadioButton retrieveRadioButton(View v) {
        RadioButton radioButton = (RadioButton) v.getTag(R.id.TAG_LANGUAGE_DIALOG_CHECKBOX_ID);

        if (radioButton == null)
            return (RadioButton) v;
        else
            return radioButton;
    }

    public void unCheckPreviousViews() {
        for (int i = 0; i < mListView.getChildCount(); i++) {
            RadioButton button = (RadioButton) mListView.getChildAt(i).findViewById(R.id.radioButton);
            button.setChecked(false);
        }
    }

    class ViewHolder {
        public LinearLayout mDialogRowLayout;
        public TextView mCategoryTextView;
        public RadioButton mRadioButtonView;
    }

}
