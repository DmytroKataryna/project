package io.asphalte.android.adapters;

import android.app.Activity;
import android.view.View;

import io.asphalte.android.Constants;
import io.asphalte.android.models.Post;

import java.util.ArrayList;

/**
 * Created by dmytro on 11/26/15. Project : Asphalte
 */
public class ProfilePostsAdapter extends PostsAdapter {

    public ProfilePostsAdapter(ArrayList<Post> posts, Activity fragment) {
        super(posts, fragment);
        mRemovePosts = false;
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.byTextView.setText(Constants.EMPTY);
        holder.byTextView.setVisibility(View.INVISIBLE);
        holder.userNameTextView.setVisibility(View.INVISIBLE);
        holder.postStatusTextView.setVisibility(View.GONE);

        holder.postLayout.setTag(post);
        holder.saveImageView.setTag(post);
        holder.userNameTextView.setTag(post);
    }
}
