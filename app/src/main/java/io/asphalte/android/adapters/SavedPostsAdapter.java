package io.asphalte.android.adapters;

import android.support.v4.app.Fragment;
import android.view.View;

import io.asphalte.android.models.Post;

import java.util.ArrayList;

/**
 * Created by dmytro on 11/26/15. Project : Asphalte
 */
public class SavedPostsAdapter extends PostsAdapter {

    public SavedPostsAdapter(ArrayList<Post> posts, Fragment fragment) {
        super(posts, fragment);
        mRemovePosts = true;
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.userNameTextView.setText(String.format("@%s", post.getOwnerUsername()));
        holder.postStatusTextView.setVisibility(View.GONE);

        holder.postLayout.setTag(post);
        holder.saveImageView.setTag(post);
        holder.userNameTextView.setTag(post);
    }
}
