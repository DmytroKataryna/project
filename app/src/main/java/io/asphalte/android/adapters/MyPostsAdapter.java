package io.asphalte.android.adapters;

import android.support.v4.app.Fragment;
import android.view.View;

import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.models.Post;

import java.util.ArrayList;

/**
 * Created by dmytro on 11/26/15. Project : Asphalte
 */
public class MyPostsAdapter extends PostsAdapter {

    public MyPostsAdapter(ArrayList<Post> posts, Fragment fragment) {
        super(posts, fragment);
        mRemovePosts = false;
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.byTextView.setText(Constants.EMPTY);
        holder.byTextView.setVisibility(View.INVISIBLE);
        holder.userNameTextView.setVisibility(View.INVISIBLE);

        holder.postStatusTextView.setVisibility(View.VISIBLE);
        holder.postStatusTextView.setText(post.isPrivate() ? mContext.getResources().getString(R.string.private_text) : mContext.getResources().getString(R.string.public_text));

        holder.postLayout.setTag(post);
        holder.saveImageView.setTag(post);
        holder.userNameTextView.setTag(post);
    }
}
