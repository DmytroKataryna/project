package io.asphalte.android.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import io.asphalte.android.Constants;
import io.asphalte.android.R;
import io.asphalte.android.helpers.TimeAgo;
import io.asphalte.android.models.Post;
import io.asphalte.android.models.User;
import io.asphalte.android.ui.LoginAlertDialogActivity;
import io.asphalte.android.ui.fragments.AbstractFragment;
import io.asphalte.android.ui.post.OwnPostDetailsActivity;
import io.asphalte.android.ui.post.PostDetailsActivity;
import io.asphalte.android.ui.profile.UserProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmytro on 11/23/15. Project : Asphalte
 */
public abstract class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder> implements View.OnClickListener {

    protected User user;
    protected Context mContext;
    protected Fragment fragment;
    protected Activity activity;
    protected List<Post> mData;
    protected Post post;

    protected boolean mMustReset;
    protected boolean mRemovePosts;

    public PostsAdapter(ArrayList<Post> posts, Fragment context) {
        this(posts);
        mContext = context.getActivity().getApplicationContext();
        fragment = context;
        activity = fragment.getActivity();
    }

    public PostsAdapter(ArrayList<Post> posts, Activity actv) {
        this(posts);
        mContext = actv.getApplicationContext();
        activity = actv;
        fragment = null;

    }

    private PostsAdapter(ArrayList<Post> posts) {
        if (posts == null)
            posts = new ArrayList<>();
        mData = posts;
        user = new User(ParseUser.getCurrentUser());
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_row, parent, false);
        return new PostViewHolder(itemView);
    }

    public void onBindViewHolder(PostViewHolder holder, int position) {
        post = mData.get(position);
        setPostTitle(post, holder.titleTextView);
        holder.postTextView.setText(post.getText());
        holder.dateTextView.setText(TimeAgo.toDuration(mContext, post.getCreatedAt()));
        changeSaveImageViewLayout(post, holder.saveImageView);
    }

    protected void setPostTitle(Post post, TextView titleTextView) {
        if (post.getTitle() != null) {
            titleTextView.setVisibility(View.VISIBLE);
            titleTextView.setText(post.getTitle());
        } else
            titleTextView.setVisibility(View.GONE);
    }

    protected void changeSaveImageViewLayout(Post post, ImageView imageView) {
        if (user.checkIfSelfPost(post)) {
            imageView.setVisibility(View.GONE);
        } else {
            imageView.setVisibility(View.VISIBLE);
            if (!isAuthorized()) return;
            changeSaveViewColor(imageView, !user.checkIfPostSaved(post));
        }
    }

    protected boolean isAuthorized() {
        return user.checkIfAuthorized();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void remove(Post post) {
        if (mRemovePosts)
            removeAt(mData.indexOf(post));
    }

    public void removeAt(int position) {
        if (position < Constants.ZERO) return;
        mData.remove(position);
        changeEmptyViewText();
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mData.size());
    }

    private void changeEmptyViewText() {
        if (mData.size() == Constants.ZERO && fragment != null)
            ((AbstractFragment) fragment).setEmptyText();
    }

    public void requestItemsRemoving() {
        mMustReset = true;
    }

    public void updateData(List<Post> deals) {
        if (deals != null) {
            if (mMustReset) {
                mData = deals;
                mMustReset = false;
            } else {
                if (mData == null)
                    mData = new ArrayList<>();
                mData.addAll(deals);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        v.setEnabled(false);
        final Post post = (Post) v.getTag();
        switch (v.getId()) {
            case R.id.saveImageView:
                saveViewInteraction(user, post, (ImageView) v);
                break;
            case R.id.userNameTextView:
                v.setEnabled(true);
                if (!user.checkIfSelfPost(post))
                    startProfileActivity(post);
                break;
            case R.id.postRowLayout:
                v.setEnabled(true);
                startPostDetailsActivity(post);
                break;
        }
    }

    private void startProfileActivity(Post post) {
        mContext.startActivity(new Intent(mContext, UserProfile.class)
                .putExtra(Constants.PARSE.OBJECT_ID, post.getOwnerID())
                .putExtra(Constants.PARSE.NAME, post.getOwnerUsername())
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private void startPostDetailsActivity(Post post) {
        Intent intent;
        if (user.checkIfSelfPost(post))
            intent = new Intent(mContext, OwnPostDetailsActivity.class);
        else
            intent = new Intent(mContext, PostDetailsActivity.class);

        mContext.startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(Constants.PARSE.OBJECT_ID, post.getObjectId())
                .putExtra(Constants.PARSE.NAME, post.getOwnerUsername()));
    }

    protected void saveViewInteraction(final User user, final Post post, final ImageView view) {
        if (!isAuthorized()) {
            showLoginDialog();
            view.setEnabled(true);
            return;
        }

        final boolean postSaved = user.checkIfPostSaved(post);
        user.savePost(post, postSaved, new SavePostCallback(post, view, postSaved));
    }

    protected void changeSaveViewColor(ImageView v, boolean dealSaved) {
        int resId = dealSaved ? R.drawable.btn_like : R.drawable.btn_liked;
        Picasso.with(mContext).load(resId).placeholder(resId).into(v);
    }

    private void showLoginDialog() {
        ((LoginAlertDialogActivity) activity).showLoginDialog();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {

        protected final ImageView saveImageView;
        protected final LinearLayout postLayout;
        protected final TextView titleTextView, postTextView, dateTextView, userNameTextView, postStatusTextView, byTextView;

        protected Typeface robotoFont;

        public PostViewHolder(View itemView) {
            super(itemView);
            postLayout = (LinearLayout) itemView.findViewById(R.id.postRowLayout);
            saveImageView = (ImageView) itemView.findViewById(R.id.saveImageView);
            titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
            postTextView = (TextView) itemView.findViewById(R.id.postTextView);
            dateTextView = (TextView) itemView.findViewById(R.id.dateTextView);
            userNameTextView = (TextView) itemView.findViewById(R.id.userNameTextView);
            postStatusTextView = (TextView) itemView.findViewById(R.id.postStatusTextView);
            byTextView = (TextView) itemView.findViewById(R.id.byTextView);

            saveImageView.setOnClickListener(PostsAdapter.this);
            userNameTextView.setOnClickListener(PostsAdapter.this);
            postLayout.setOnClickListener(PostsAdapter.this);

            robotoFont = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Regular.ttf");
            titleTextView.setTypeface(Typeface.SERIF, Typeface.BOLD);
            postTextView.setTypeface(Typeface.SERIF);
            postStatusTextView.setTypeface(robotoFont);
            userNameTextView.setTypeface(robotoFont);
            byTextView.setTypeface(robotoFont);
        }
    }

    public class SavePostCallback implements SaveCallback {

        private boolean postSaved;
        private Post currentPost;
        private ImageView view;

        public SavePostCallback(final Post post, final ImageView v, final boolean postSaved) {
            this.currentPost = post;
            this.view = v;
            this.postSaved = postSaved;
        }

        @Override
        public void done(ParseException e) {
            user.sendPushNotification(currentPost, postSaved);
            view.setEnabled(true);
            changeSaveViewColor(view, postSaved);
            remove(currentPost);
        }
    }
}